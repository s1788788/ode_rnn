from scipy.integrate import solve_ivp
import numpy as np
from matplotlib import pyplot as plt
import torch
import math

from torch.autograd import Function
from torch.distributions.uniform import Uniform
from torch import nn

from codebase_bartol_parallel import *
from ODE_code_bartol import *

import time

#torch.manual_seed(10)
#np.random.seed(10)


###################################
######### INDICATOR CONCS ######### 
###################################

def return_init(which, conc):

    if which == 'Fluo4' and conc == '20':

        Ca_0        =   0.1  ##uM
        Fluo4_0     =  15.2  ##uM
        CaFluo4_0   =   4.8  ##uM
        OGB1_0      =   0.0  ##uM
        CaOGB1_0    =   0.0  ##uM
        IMCBP_0     =  75.5  ##uM
        CaIMCBP_0   =   3.5  ##uM
        NCX_0       =   2.0  ##uM
        CaNCX_0     =   1.0  ##uM
        PMCA_0      =  18.4  ##uM
        CaPMCA_0    =   0.6  ##uM


    elif which == 'OGB1' and conc == '20':

        Ca_0        =   0.1  ##uM
        Fluo4_0     =   0.0  ##uM
        CaFluo4_0   =   0.0  ##uM
        OGB1_0      =  13.5  ##uM
        CaOGB1_0    =   6.5  ##uM
        IMCBP_0     =  75.5  ##uM
        CaIMCBP_0   =   3.5  ##uM
        NCX_0       =   2.0  ##uM
        CaNCX_0     =   1.0  ##uM
        PMCA_0      =  18.4  ##uM
        CaPMCA_0    =   0.6  ##uM


    elif which == 'OGB1' and conc == '50':

        Ca_0        =   0.1  ##uM
        Fluo4_0     =   0.0  ##uM
        CaFluo4_0   =   0.0  ##uM
        OGB1_0      =  33.8  ##uM
        CaOGB1_0    =  16.2  ##uM
        IMCBP_0     =  75.5  ##uM
        CaIMCBP_0   =   3.5  ##uM
        NCX_0       =   2.0  ##uM
        CaNCX_0     =   1.0  ##uM
        PMCA_0      =  18.4  ##uM
        CaPMCA_0    =   0.6  ##uM


    elif which == 'OGB1' and conc == '100':

        Ca_0        =   0.1  ##uM
        Fluo4_0     =   0.0  ##uM
        CaFluo4_0   =   0.0  ##uM
        OGB1_0      =  67.6  ##uM
        CaOGB1_0    =  32.4  ##uM
        IMCBP_0     =  75.5  ##uM
        CaIMCBP_0   =   3.5  ##uM
        NCX_0       =   2.0  ##uM
        CaNCX_0     =   1.0  ##uM
        PMCA_0      =  18.4  ##uM
        CaPMCA_0    =   0.6  ##uM


    c_0     = np.array([Ca_0, Fluo4_0, CaFluo4_0, OGB1_0, CaOGB1_0, IMCBP_0, CaIMCBP_0, NCX_0, CaNCX_0, PMCA_0, CaPMCA_0])
    c_0_rnn = np.array([Ca_0, Fluo4_0, CaFluo4_0, OGB1_0, CaOGB1_0, IMCBP_0, CaIMCBP_0, 0])

    return c_0, c_0_rnn


##################################
#### LOADING/SAVING FUNCTIONS ####
##################################

def save_data(T, ODE_INIT, RNN_INIT, INS, OUTS, RNN_FLUO, name):
    
    with open('{}_t.npy'.format(name), 'wb') as f:
        np.save(f, T)

    with open('{}_ode_inits.npy'.format(name), 'wb') as f:
        np.save(f, ODE_INIT)

    with open('{}_rnn_inits.npy'.format(name), 'wb') as f:
        np.save(f, RNN_INIT)

    with open('{}_ins.npy'.format(name), 'wb') as f:
        np.save(f, INS)
    
    with open('{}_outs.npy'.format(name), 'wb') as f:
        np.save(f, OUTS)

    with open('{}_rnn_fluo.npy'.format(name), 'wb') as f:
        np.save(f, RNN_FLUO)

    return 0



def load_data(name):

    with open('{}_t.npy'.format(name), 'rb') as f:
        t = np.load(f)

    with open('{}_ode_inits.npy'.format(name), 'rb') as f:
        ode_inits = np.load(f)

    with open('{}_rnn_inits.npy'.format(name), 'rb') as f:
        rnn_inits = np.load(f)

    with open('{}_ins.npy'.format(name), 'rb') as f:
        ins = np.load(f)

    with open('{}_outs.npy'.format(name), 'rb') as f:
        outs = np.load(f)

    with open('{}_rnn_fluo.npy'.format(name), 'rb') as f:
        rnn_fluo = np.load(f)

    return t, ode_inits, rnn_inits, ins, outs, rnn_fluo

##################################
#### LOADING/SAVING FUNCTIONS ####
##################################



def f_bartol_scipy(t, y, p, c):

    k1f = 0.8   * 0.01
    k1b = 0.24  * 0.01
   
    k2f = 0.8   * 0.01
    k2b = 0.16  * 0.01
    
    k3f = 0.247 * 0.01
    k3b = 0.524 * 0.01

    k4f = p[0] * 0.01
    k4b = p[1] * 0.01
    k4p = p[2] * 0.01
    k4l = p[3] * 0.01
   
    k5f = p[4] * 0.01
    k5b = p[5] * 0.01
    k5p = p[6] * 0.01
    k5l = p[7] * 0.01

    Ca      = y[0]
    Fluo4   = y[1]
    CaFluo4 = y[2]
    OGB1    = y[3]
    CaOGB1  = y[4]
    IMCBP   = y[5]
    CaIMCBP = y[6]
    NCX     = y[7]
    CaNCX   = y[8]
    PMCA    = y[9]
    CaPMCA  = y[10]

    

    dCa      = -k1f * Ca * Fluo4 * c + k1b * CaFluo4 - k2f * Ca * OGB1 * c + k2b * CaOGB1 - k3f * Ca * IMCBP * c + k3b * CaIMCBP - k4f * Ca * NCX * c + k4b * CaNCX + k4l * NCX - k5f * Ca * PMCA * c + k5b * CaPMCA + k5l * PMCA
    dFluo4   = -k1f * Ca * Fluo4 * c + k1b * CaFluo4
    dCaFluo4 =  k1f * Ca * Fluo4 * c - k1b * CaFluo4
    dOGB1    = -k2f * Ca * OGB1  * c + k2b * CaOGB1 
    dCaOGB1  =  k2f * Ca * OGB1  * c - k2b * CaOGB1 
    dIMCBP   = -k3f * Ca * IMCBP * c + k3b * CaIMCBP
    dCaIMCBP =  k3f * Ca * IMCBP * c - k3b * CaIMCBP
    dNCX     = -k4f * Ca * NCX   * c + (k4b + k4p) * CaNCX
    dCaNCX   =  k4f * Ca * NCX   * c - (k4b + k4p) * CaNCX
    dPMCA    = -k5f * Ca * PMCA  * c + (k5b + k5p) * CaPMCA
    dCaPMCA  =  k5f * Ca * PMCA  * c - (k5b + k5p) * CaPMCA


    return np.array([dCa, dFluo4, dCaFluo4, dOGB1, dCaOGB1, dIMCBP, dCaIMCBP, dNCX, dCaNCX, dPMCA, dCaPMCA])


from scipy.optimize import minimize
from scipy.integrate import simps

#rates = {'k1f' : 0.8   * t_scale, 'k1b' : 0.24  * t_scale, 
#         'k2f' : 0.8   * t_scale, 'k2b' : 0.16  * t_scale,
#         'k3f' : 0.247 * t_scale, 'k3b' : 0.524 * t_scale,
#         'k4f' : 0.15  * t_scale, 'k4b' : 0.015 * t_scale, 'k4p' : 0.012 * t_scale, 'k4l' : 0.0043 * t_scale,
#         'k5f' : 0.3   * t_scale, 'k5b' : 0.3   * t_scale, 'k5p' : 0.6   * t_scale, 'k5l' : 0.0194 * t_scale}

c_scale = 75.5 
t_scale = 0.01
dt = 0.01/t_scale
sim_time = 100/t_scale


def RK45_with_init_integration(system, y0, t_max, dt, params, c_scale, index, inp, out):

        T, _, Y = RK45_with_input(system = system, y0 = y0, t_max = t_max, dt = dt, evals = None, params = params, c_scale = c_scale, index = index, inp = inp) 

        if Y[1,0] == 0: ## Fluo4 is 0, use OGB1
            sq_err = simps(y = np.square(out[4,:] - Y[4,:]), x = T)
        else:
            sq_err = simps(y = np.square(out[2,:] - Y[2,:]), x = T)

        return sq_err, Y



def func_to_min(rr, inits, ins, outs, T_MAX = sim_time, DT = dt):

    stuff = Parallel(n_jobs=15)(delayed(RK45_with_init_integration)(system = f_bartol_scipy, y0 = inits[i], t_max = T_MAX, dt = DT, params = rr, c_scale = c_scale, index = None, inp = ins[i], out=outs[i]) for i in range(len(inits)))

    errors = [thing[0] for thing in stuff]

    total_err = np.sum(errors)

    return total_err



def func_min_eps_batches(inits, ins, outs, N_EPS, N_BATCHES, rr_init):

#    inits = inits[:100]
#    ins   = ins[:100]
#    outs  = outs[:100]

    idx = np.arange(0, len(inits), 1)
    np.random.shuffle(idx)
    shuffled_idxs = np.array_split(idx, N_BATCHES)

    x0 = rr_init

    error_vals_eps          = []
    error_vals_batches      = []

    best_err = 1E6
    best_rates = None

    for ep in range(N_EPS):

        error_vals_batches_temp      = []

        for i in range(len(shuffled_idxs)):
        #for i in range(2):
            idx = shuffled_idxs[i]

            #stuff = minimize(fun = func_to_min, x0 = x0, options = {'maxiter':1, 'disp':True}, bounds = [(0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1)], method='L-BFGS-B', args = (inits[idx], ins[idx], outs[idx]))
            stuff = minimize(fun = func_to_min, x0 = x0, options = {'maxiter':1, 'disp':False}, bounds = [(0, 1), (0, 1), (0, 1), (0, 0.1), (0, 1), (0, 1), (0, 1), (0, 0.1)], method='L-BFGS-B', args = (inits[idx], ins[idx], outs[idx]))

            x0 = stuff['x']
#            print(x0)
            error_vals_batches_temp.append(stuff['fun'])

            #print('SQUARE ERROR AT ITERATION {} IS {}'.format(shuffled_idxs.index(idx) + 1, stuff['fun']))

            print('EP {}/{}. SQUARE ERROR AT ITERATION {}/{} IS {}'.format(ep + 1, N_EPS, i + 1, len(shuffled_idxs), stuff['fun']))

            if stuff['fun'] < best_err:
                best_err = stuff['fun']
                best_rates = stuff['x']


        error_vals_batches.append(error_vals_batches_temp)
        error_vals_eps.append(np.sum(error_vals_batches_temp))

    last_rates = stuff['x']


    return error_vals_eps, error_vals_batches, last_rates, best_rates




#t, train_ode_inits, train_rnn_inits, train_ins, train_outs, train_rnn_fluo_init = load_data('./Data/train')
#
##train_ode_inits = train_ode_inits[:10]
##train_outs = train_outs[:10]
##train_ins  = train_ins[:10]
#
#x0 = np.random.uniform(low=0, high=1, size = 8) * t_scale
#
#print('STARTING L-BFGS-B OPTIMIZATION')
#
#stuff = minimize(fun = func_to_min, x0 = x0, options = {'maxiter':30, 'disp':True, 'ftol' : 0.001}, bounds = [(0, 1), (0, 1), (0, 1), (0, 0.1), (0, 1), (0, 1), (0, 1), (0, 0.1)], method='L-BFGS-B', args = (train_ode_inits, train_ins, train_outs))
#
#
#print(stuff['x'])





##x0 = np.array([0.15, 0.015, 0.012, 0.0043, 0.3, 0.3, 0.6, 0.0194]) * t_scale ##TRUE RATES

#eps, batches, last_rates, best_rates = func_min_eps_batches(inits = train_ode_inits, ins = train_ins, outs = train_outs, N_EPS = 4, N_BATCHES = 100, rr_init = x0)
#eps, batches, last_rates, best_rates = func_min_eps_batches(inits = train_ode_inits, ins = train_ins, outs = train_outs, N_EPS = 1, N_BATCHES = 10, rr_init = x0)

#print(batches)
#print(eps)


#with open('scipy_RMSE.npy', 'wb') as f:
#    np.save(f, batches)


#with open('scipy_rates.npy', 'wb') as f:
#    np.save(f, np.vstack([last_rates, best_rates]))


#with open('scipy_RMSE.npy', 'rb') as f:
#    batches = np.load(f, allow_pickle=True)

#with open('scipy_rates.npy', 'rb') as f:
#    rates = np.load(f)
#
#print(rates)



##########################
#### VALIDATION STUFF ####
##########################


def RK45_val_evals(rr, inits, ins, outs, T_MAX = sim_time, DT = dt, index=None):

    if index == 'i':
        stuff = Parallel(n_jobs=15)(delayed(RK45_with_init_integration)(system = f_bartol_scipy, y0 = inits[i], t_max = T_MAX, dt = DT, params = rr, c_scale = c_scale, index = i, inp = ins[i], out=outs[i]) for i in range(len(inits)))
    else:
        stuff = Parallel(n_jobs=15)(delayed(RK45_with_init_integration)(system = f_bartol_scipy, y0 = inits[i], t_max = T_MAX, dt = DT, params = rr, c_scale = c_scale, index = None, inp = ins[i], out=outs[i]) for i in range(len(inits)))

    errors  = [thing[0] for thing in stuff]
    Ys      = [thing[1] for thing in stuff]

    return errors, Ys




def RMSE_sampling(rr, all_inits, all_ins, all_outs, num_runs, sample_size, name=None):

    idx = np.arange(0, len(all_inits), 1)

    all_RMSEs = []

    for i in range(num_runs):

        t0 = time.time()
        i_idx = np.random.choice(idx, sample_size, replace=True)

        i_inits = [all_inits[j_idx] for j_idx in i_idx]
        i_ins   = [all_ins[j_idx]   for j_idx in i_idx]
        i_outs  = [all_outs[j_idx]  for j_idx in i_idx]

        i_errs, _ = RK45_val_evals(rr = rr, inits = i_inits, ins = i_ins, outs = i_outs)

        all_RMSEs.append(i_errs)

        t1 = time.time()

        print('Validation run {}/{}. Took {:.3E} sec'.format(i+1, num_runs, t1-t0))


    with open('./' + name + '.npy', 'wb') as f:
        np.save(f, all_RMSEs)

    return 0





#print(np.sqrt(np.array([np.sum(bat) for bat in batches])/300))

#plt.plot(np.log10(np.sqrt(np.array([np.sum(bat) for bat in batches])/300)))
#plt.show()


#print(x0)
#stuff = minimize(fun = func_to_min, x0 = x0, options = {'maxiter':1, 'disp':True}, bounds = [(0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1)], method='L-BFGS-B')
#
#x0 = stuff['x']
#
##print(stuff['fun'])
#
#
#print(x0)
#stuff = minimize(fun = func_to_min, x0 = x0, options = {'maxiter':1, 'disp':True}, bounds = [(0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1), (0, 1)], method='L-BFGS-B')




#x_opt = np.array([0.30884191, 0.16373515, 0.30833507, 0.01719671, 0.31023022, 0.16699752, 0.30714586, 0.02005671]) ### WITH 30 BATCHS OF 100, 5 ITERATIONS

x_opt = np.array([0.2504377, 0.00983101, 0.25834945, 0.02762579, 0.24084822, 0.07609164, 0.26433883, 0.01737209]) ### WITH FULL 3K DATA PTS AS A SINGLE BATCH, 30 ITERATIONS

#####################
##### TEST DATA #####
#####################

t, test_ode_inits, test_rnn_inits, test_ins, test_outs, test_rnn_fluo_init = load_data('./Data/RMSE_test')
RMSE_sampling(rr = x_opt, all_inits = test_ode_inits, all_ins = test_ins, all_outs = test_outs, num_runs = 50, sample_size = 300, name='scipy_test_RMSEs')


#####################
###### GEN DATA #####
#####################

t, gen_ode_inits, gen_rnn_inits, gen_ins, gen_outs, gen_rnn_fluo_init = load_data('./Data/RMSE_gen')
RMSE_sampling(rr = x_opt, all_inits = gen_ode_inits, all_ins = gen_ins, all_outs = gen_outs, num_runs = 50, sample_size = 300, name='scipy_gen_RMSEs')



#######################
###### OGB1 RANGE #####
#######################


t, r1_ode_inits, r1_rnn_inits, r1_ins, r1_outs, r1_rnn_fluo_init = load_data('./Data/range_OGB1')

###r1_ode_inits    = r1_ode_inits[:10]
###r1_ins          = r1_ins[:10]
###r1_outs         = r1_outs[:10]

r1_errors, _ = RK45_val_evals(rr = x_opt, inits = r1_ode_inits, ins = r1_ins, outs = r1_outs, T_MAX = sim_time, DT = dt, index='i')
with open('./scipy_OGB1_range_RMSE.npy', 'wb') as f:
    np.save(f, r1_errors)




######################
##### FLUO4 RANGE ####
######################

t, r2_ode_inits, r2_rnn_inits, r2_ins, r2_outs, r2_rnn_fluo_init = load_data('./Data/range_Fluo4')


r2_errors, _ = RK45_val_evals(rr = x_opt, inits = r2_ode_inits, ins = r2_ins, outs = r2_outs, T_MAX = sim_time, DT = dt, index='i')
with open('./scipy_Fluo4_range_RMSE.npy', 'wb') as f:
    np.save(f, r2_errors)


#####################
#### SAMPLE DATA ####
#####################

#x0 = np.random.uniform(low=0, high=1, size = 8) * t_scale
#t, val_ode_inits, val_rnn_inits, val_ins, val_outs, val_rnn_fluo_init = load_data('./Data/val')
#
#
#val_errors, val_outs = RK45_val_evals(rr = x0, inits = val_ode_inits[:2], ins = val_ins[:2], outs = val_outs[:2], T_MAX = sim_time, DT = dt, index='i')
#plt.plot(val_outs[0][4,:])
#plt.show()
#
#with open('./plot_stuff3.npy', 'wb') as f:
#    np.save(f, val_outs[0][4,:])

