import numpy as np
from matplotlib import pyplot as plt
import torch



layers  = 2 
units   = 100
eps     = 10
batches = 100
b_size  = 30
lr      = 0.01
wlam    = 0.0
schstep = 50
gamma   = 0.7


hybrid_name = 'hybrid_model_NLAYER_{}_NH_{}_NEPS_{}_NBATCHES_{}_BATCHSIZE_{}_LR_{}_WLAM_{}_SCHSTEP_{}_SCHGAMMA_{}'.format(layers, units, eps, batches, b_size, lr, wlam, schstep, gamma)


hybrid_stuff = torch.load('./Models/' + hybrid_name + '.pt')


with open('./RMSEs/' + hybrid_name + '_RMSE_val.npy', 'rb') as f:
    hybrid_RMSEs = np.load(f)
with open('./RMSEs/' + hybrid_name + '_RMSE_test.npy', 'rb') as f:
    hybrid_test_RMSEs = np.load(f)
with open('./RMSEs/' + hybrid_name + '_RMSE_gen.npy', 'rb') as f:
    hybrid_gen_RMSEs = np.load(f)
with open('./RMSEs/' + hybrid_name + '_range_OGB1.npy', 'rb') as f:
    hybrid_range_OGB1 = np.load(f)
with open('./RMSEs/' + hybrid_name + '_range_Fluo4.npy', 'rb') as f:
    hybrid_range_Fluo4 = np.load(f)



layers  = 2 
units   = 100
eps     = 10
batches = 100
b_size  = 30
lr      = 0.001
wlam    = 0.0
schstep = 50
gamma   = 0.8


rnn_name    = 'RNN_baseline_model_NLAYER_{}_NH_{}_NEPS_{}_NBATCHES_{}_BATCHSIZE_{}_LR_{}_WLAM_{}_SCHSTEP_{}_SCHGAMMA_{}'.format(layers, units, eps, batches, b_size, lr, wlam, schstep, gamma)

rnn_stuff    = torch.load('./Models/' + rnn_name + '.pt')

with open('./RMSEs/' + rnn_name + '_RMSE_val.npy', 'rb') as f:
    rnn_RMSEs = np.load(f)
with open('./RMSEs/' + rnn_name + '_RMSE_test.npy', 'rb') as f:
    rnn_test_RMSEs = np.load(f)
with open('./RMSEs/' + rnn_name + '_RMSE_gen.npy', 'rb') as f:
    rnn_gen_RMSEs = np.load(f)
with open('./RMSEs/' + rnn_name + '_range_OGB1.npy', 'rb') as f:
    rnn_range_OGB1 = np.load(f)
with open('./RMSEs/' + rnn_name + '_range_Fluo4.npy', 'rb') as f:
    rnn_range_Fluo4 = np.load(f)



#axs = plt.figure(figsize = (6.5, 3.1)).subplots(2, 2)
#axs = axs.flat

#axs = plt.figure(figsize = (5.5, 3.1)).subplots(1, 1)

### THREE GROUP COLORBLIND FRIENDLY SCHEME ###
#c1 = '#1b9e77'
#c2 = '#d95f02'
#c3 = '#7570b3'



### FOUR GROUP COLORBLIND FRIENDLY SCHEME ###
c1 = '#a6cee3'
c2 = '#1f78b4'
c3 = '#b2df8a'
c4 = '#33a02c'

#plt.subplots_adjust(top=0.93, left = 0.15, right = 0.995, bottom = 0.135, hspace = 0.75, wspace = 0.30)


#####################
##### SUBPLOT 1 #####
#####################

#with open('plot_stuff2.npy', 'rb') as f:
#    stuff = np.load(f)
#
#with open('plot_stuff3.npy', 'rb') as f:
#    stuff3 = np.load(f)
#
#ax = axs[0]
#
#ax.set_title('1. Sample Untrained CaOGB1 Dynamics', fontweight = 'bold', fontsize=10)
#
#t = np.arange(0, 100.01, 0.01)
#
#ax.plot(t, stuff[0,:] * 75.5, color=c3, alpha=0.8, label='True',    linewidth=2)
#ax.plot(t, stuff[1,:] * 75.5, color=c1, alpha=0.8, label='Hybrid',  linewidth=2)
#ax.plot(t, stuff[2,:] * 75.5, color=c2, alpha=0.8, label='RNN',     linewidth=2)
#ax.plot(t, stuff3     * 75.5, color=c4, alpha=0.8, label='SciPy',   linewidth=2)
#
#ax.set_ylim([-20, 120])
##ax.set_xlim([0, 3])
##ax.set_xticks([1, 2])
##ax.set_xticklabels(labels)
#
#ax.tick_params(axis='y', labelsize=7)
#ax.tick_params(axis='x', labelsize=7)
#
#ax.set_xlabel('Time (ms)')#, fontweight = 'bold')
#ax.set_ylabel('Conc.($\mu M$)') #, fontweight = 'bold')
#
#ax.legend(prop={'size':7}, frameon=False, loc='upper left')

#####################
##### SUBPLOT 2 #####
#####################

#ax = axs[1]
#ax.set_title('2. Model training', fontweight = 'bold', fontsize=10)

#ax = axs
#ax.set_title('Training errors', fontweight = 'bold', fontsize=12)
#
#ax.plot(np.log10(np.sqrt(np.array(hybrid_stuff['train_loss']) / 30)), '-s', label='ODE-RNN', linewidth=2, alpha = 0.7, markersize = 8, color = c1)
#ax.plot(np.log10(np.sqrt(np.array(rnn_stuff['train_loss'])    / 30)), '-^', label='sRNN', linewidth=2, alpha = 0.7, markersize = 8, color = c2)
#
#
#sci_err = np.array([1.9E5, 1.9E4, 1.28E4, 8.472E3, 6.20E3, 2.55E3, 4.14E2, 1.239E2, 2.47E1, 2.2E1, 2.19E1, 2.15E1, 2.06E1, 1.92E1, 1.74E1, 1.43E1, 1.41E1, 1.16E1, 1.05E1, 9.56E0, 8.95E0, 5.549E0, 8.30E0, 8.096E0, 7.8E0, 7.595E0, 7.3E0, 6.94E0, 6.66E0])
#ax.plot(np.log10(np.sqrt(sci_err/3000))[:10], '-o', label = 'L-BFGS-B', linewidth=2, alpha=0.7, markersize = 8, color=c4)
#
#
#
#ax.set_xlabel('Epoch') #, fontweight = 'bold')
#ax.set_ylabel(r'Log$_{10}$(RMSE)') #, fontweight = 'bold')
#
#ax.tick_params(axis='x', labelsize=8)
#ax.tick_params(axis='y', labelsize=8)
#
#ax.set_ylim([-1.2, 1.0])
#
#ax.legend(prop={'size':8}, frameon=False)




#####################
##### SUBPLOT 3 #####
#####################

#with open('scipy_test_RMSEs.npy', 'rb') as f:
#    scipy_test_RMSEs = np.load(f)
#    scipy_test_RMSEs = [np.sqrt(np.sum(rmse)/300) for rmse in scipy_test_RMSEs] 
#
#
#with open('scipy_gen_RMSEs.npy', 'rb') as f:
#    scipy_gen_RMSEs = np.load(f)
#    scipy_gen_RMSEs = [np.sqrt(np.sum(rmse)/300) for rmse in scipy_gen_RMSEs] 
#
#ax = axs
#ax.set_title('Training errors', fontweight = 'bold', fontsize=12)
#
###ax = axs[2]
###ax.set_title('3. Generalization to Input Types', fontweight = 'bold', fontsize=10)
#
#bp1 = ax.boxplot(np.log10(np.vstack([hybrid_test_RMSEs, hybrid_gen_RMSEs])).T, widths=0.5, medianprops = {'color':c1})
#bp2 = ax.boxplot(np.log10(np.vstack([rnn_test_RMSEs, rnn_gen_RMSEs])).T, widths=0.5, medianprops = {'color':c2})
#bp3 = ax.boxplot(np.log10(np.vstack([scipy_test_RMSEs, scipy_gen_RMSEs])).T, widths=0.5, medianprops = {'color':c4})

#bp1 = ax.boxplot(np.log(np.vstack([hybrid_test_RMSEs, hybrid_gen_RMSEs])).T, widths=0.5, medianprops = {'color':c1})
##bp2 = ax.boxplot(np.log(np.vstack([rnn_test_RMSEs, rnn_gen_RMSEs])).T, widths=0.5, medianprops = {'color':c2})
##bp3 = ax.boxplot(np.log(np.vstack([scipy_test_RMSEs, scipy_gen_RMSEs])).T, widths=0.5, medianprops = {'color':c4})


##bp1 = ax.boxplot((np.vstack([hybrid_test_RMSEs, hybrid_gen_RMSEs])).T, widths=0.5, medianprops = {'color':c1})
##bp2 = ax.boxplot((np.vstack([rnn_test_RMSEs, rnn_gen_RMSEs])).T, widths=0.5, medianprops = {'color':c2})
##bp3 = ax.boxplot((np.vstack([scipy_test_RMSEs, scipy_gen_RMSEs])).T, widths=0.5, medianprops = {'color':c4})


##ax.legend([bp1['boxes'][0], bp2['boxes'][0]], ['A', 'B'], loc='upper right')
#
#ax.legend([bp1['medians'][0], bp2['medians'][0], bp3['medians'][0]], ['Hybrid', 'RNN', 'SciPy'], prop={'size':7}, frameon=False)
#
#
#labels = [item.get_text() for item in ax.get_xticklabels()]
#print(labels)
#labels[0] = r'Ca$^{2+}$ trains'
#labels[2] = r'Ca$^{2+}$ trains'
#labels[4] = r'Ca$^{2+}$ trains'
#labels[1] = r'Ca$^{2+}$ currents'
#labels[3] = r'Ca$^{2+}$ currents'
#labels[5] = r'Ca$^{2+}$ currents'

#labels[2] = 'Pulse'
#labels[3] = 'Leak'
##labels[0] = 'Hybrid/Pulse'
##labels[1] = 'RNN/Pulse'
##labels[2] = 'Hybrid/Leak'
##labels[3] = 'RNN/Leak'

#ax.set_xticklabels(labels)
#
###ax.set_xlabel('Model')#, fontweight = 'bold')
#ax.set_xlabel('Input Type')#, fontweight = 'bold')
#ax.set_ylabel(r'Log$_{10}$(RMSE)') #,  fontweight = 'bold')
#
#ax.tick_params(axis='y', labelsize=8)
#ax.tick_params(axis='x', labelsize=8)



######################
###### SUBPLOT 4 #####
######################

with open('scipy_OGB1_range_RMSE.npy', 'rb') as f:
    scipy_range_OGB1 = np.load(f)

with open('scipy_Fluo4_range_RMSE.npy', 'rb') as f:
    scipy_range_Fluo4 = np.load(f)


axs = plt.figure(figsize = (5.5, 3.1)).subplots(1, 1)
plt.subplots_adjust(top=0.90, left = 0.10, right = 0.995, bottom = 0.135, hspace = 0.75, wspace = 0.30)

ax = axs
ax.set_title('Generalization to Indicator Conc.', fontweight = 'bold', fontsize=12)

#ax = axs[3]
#ax.set_title('4. Generalization to Indicator Conc.', fontweight = 'bold', fontsize=10)


concs = np.arange(5, 505, 5)

RMSE_hybrid_OGB1 = []
RMSE_hybrid_Fluo4 = []
RMSE_rnn_OGB1 = []
RMSE_rnn_Fluo4 = []
RMSE_scipy_OGB1 = []
RMSE_scipy_Fluo4 = []

for i in range(len(concs)):
    RMSE_hybrid_OGB1.append( np.sqrt(np.sum( hybrid_range_OGB1[i * 30 : (i + 1) * 30] ) / 30))
    RMSE_rnn_OGB1.append( np.sqrt(np.sum( rnn_range_OGB1[i * 30 : (i + 1) * 30] ) / 30))
    RMSE_scipy_OGB1.append( np.sqrt(np.sum( scipy_range_OGB1[i * 30 : (i + 1) * 30] ) / 30))

    RMSE_hybrid_Fluo4.append( np.sqrt(np.sum( hybrid_range_Fluo4[i * 30 : (i + 1) * 30] ) / 30))
    RMSE_rnn_Fluo4.append( np.sqrt(np.sum( rnn_range_Fluo4[i * 30 : (i + 1) * 30] ) / 30))
    RMSE_scipy_Fluo4.append( np.sqrt(np.sum( scipy_range_Fluo4[i * 30 : (i + 1) * 30] ) / 30))

       
#ax.plot(concs, RMSE_hybrid_OGB1,  label='Hybrid/OGB1', linewidth=2, alpha=0.7, color  = c1)
#ax.plot(concs, RMSE_hybrid_Fluo4, '--', label='Hybrid/Fluo4', linewidth=2, alpha=0.7, color = c1)
#
#ax.plot(concs, RMSE_rnn_OGB1,    label='RNN/OGB1', linewidth=2, alpha=0.7,  color = c2)
#ax.plot(concs, RMSE_rnn_Fluo4, '--',    label='RNN/Fluo4', linewidth=2, alpha=0.7, color = c2)
#
#ax.plot(concs, RMSE_scipy_OGB1,  label='SciPy/OGB1', linewidth=2, alpha=0.7, color  = c4)
#ax.plot(concs, RMSE_scipy_Fluo4, '--',  label='SciPy/Fluo4', linewidth=2, alpha=0.7, color  = c4)

ax.plot(concs, np.log10(RMSE_hybrid_OGB1),  label='ODE+RNN/OGB1', linewidth=2, alpha=0.7, color  = c1)
ax.plot(concs, np.log10(RMSE_hybrid_Fluo4), '--', label='ODE+RNN/Fluo4', linewidth=2, alpha=0.7, color = c1)

ax.plot(concs, np.log10(RMSE_rnn_OGB1),    label='sRNN/OGB1', linewidth=2, alpha=0.7,  color = c2)
ax.plot(concs, np.log10(RMSE_rnn_Fluo4), '--',    label='sRNN/Fluo4', linewidth=2, alpha=0.7, color = c2)

ax.plot(concs, np.log10(RMSE_scipy_OGB1),  label='L-BFGS-B/OGB1', linewidth=2, alpha=0.7, color  = c4)
ax.plot(concs, np.log10(RMSE_scipy_Fluo4), '--',  label='L-BFGS-B/Fluo4', linewidth=2, alpha=0.7, color  = c4)


#ax.plot([20, 50, 100], [0, 0, 0], 'x', markersize = 8, mew = 3, label = 'Training data', color='black')
ax.plot([20, 50, 100], [0, 0, 0], 'x', markersize = 8, mew = 3, color='black')

#ax.set_ylim([-30, 300])
ax.set_ylim([-2, 4])


ax.set_xlabel(r'Conc. ($\rm{\mu M}$)') #, fontweight = 'bold')
ax.set_ylabel(r'Log$_{10}$(RMSE)')# ,  fontweight = 'bold')

ax.tick_params(axis='y', labelsize=8)
ax.tick_params(axis='x', labelsize=8)

ax.legend(prop={'size':9}, ncol=3, frameon=False)



###############################################
##### SUBPLOT 3 ALT WITH VALIDATION DATA  #####
###############################################

#with open('scipy_test_RMSEs.npy', 'rb') as f:
#    scipy_test_RMSEs = np.load(f)
#    scipy_test_RMSEs = [np.sqrt(np.sum(rmse)/300) for rmse in scipy_test_RMSEs] 
#
#
#with open('scipy_gen_RMSEs.npy', 'rb') as f:
#    scipy_gen_RMSEs = np.load(f)
#    scipy_gen_RMSEs = [np.sqrt(np.sum(rmse)/300) for rmse in scipy_gen_RMSEs] 

#ax = axs
#ax.set_title('Validation errors', fontweight = 'bold', fontsize=12)
#
#
#bp1 = ax.boxplot(np.log10(np.vstack([hybrid_RMSEs])).T, widths=0.5, medianprops = {'color':c1}, positions = [0])
#bp2 = ax.boxplot(np.log10(np.vstack([rnn_RMSEs])).T, widths=0.5, medianprops = {'color':c2}, positions = [1])
##bp3 = ax.boxplot(np.log10(np.vstack([rnn_RMSEs])).T, widths=0.5, medianprops = {'color':c4}, positions = [2])
#
##ax.legend([bp1['medians'][0], bp2['medians'][0]], ['ODE-RNN', 'sRNN'], prop={'size':8}, frameon=False)
#
#
#labels = [item.get_text() for item in ax.get_xticklabels()]
#labels[0] = 'ODE-RNN'
#labels[1] = 'sRNN'
##labels[2] = 'L-BFGS-B'
#
#ax.set_xticklabels(labels)
#
#ax.set_xlabel('Model Type')
#ax.set_ylabel(r'Log$_{10}$(RMSE)')
#
#ax.tick_params(axis='y', labelsize=8)
#ax.tick_params(axis='x', labelsize=8)



#########################################
##### SUBPLOT 3 ALTERNATIVE 2 PLOTS #####
#########################################
#
#axs = plt.figure(figsize = (5.5, 3.1)).subplots(1, 2)
#plt.subplots_adjust(top=0.85, left = 0.12, right = 0.995, bottom = 0.135, hspace = 0.75, wspace = 0.30)
#
#with open('scipy_test_RMSEs.npy', 'rb') as f:
#    scipy_test_RMSEs = np.load(f)
#    scipy_test_RMSEs = [np.sqrt(np.sum(rmse)/300) for rmse in scipy_test_RMSEs] 
#
#
#with open('scipy_gen_RMSEs.npy', 'rb') as f:
#    scipy_gen_RMSEs = np.load(f)
#    scipy_gen_RMSEs = [np.sqrt(np.sum(rmse)/300) for rmse in scipy_gen_RMSEs] 
#
#ax = axs[0]
#ax.set_title(r'Test error on Ca$^{2+}$' + '\nspikes', fontweight = 'bold', fontsize=12)
#
#
#ax.text(-1.3, 1.3, 'A', fontweight='bold', fontsize=11)
#
#bp1 = ax.boxplot(np.log10(np.vstack([hybrid_test_RMSEs])).T, widths=0.5, medianprops = {'color':c1}, positions=[0])
#bp2 = ax.boxplot(np.log10(np.vstack([rnn_test_RMSEs])).T, widths=0.5, medianprops = {'color':c2}, positions=[1])
#bp3 = ax.boxplot(np.log10(np.vstack([scipy_test_RMSEs])).T, widths=0.5, medianprops = {'color':c4}, positions=[2])
#
#
#
#labels = [item.get_text() for item in ax.get_xticklabels()]
#labels[0] = 'ODE+RNN'
#labels[1] = 'sRNN'
#labels[2] = 'L-BFGS-B'
#
#ax.set_xticklabels(labels)
#
#ax.set_xlabel('Model')
#ax.set_ylabel(r'Log$_{10}$(RMSE)')
#
#ax.tick_params(axis='y', labelsize=8)
#ax.tick_params(axis='x', labelsize=8)
#ax.set_ylim([-1.1, 1.1])
#
#
#
#
#
#ax = axs[1]
#ax.set_title(r'Test error on Ca$^{2+}$' + '\ncurrents', fontweight = 'bold', fontsize=12)
#
#ax.text(-1.3, 1.3, 'B', fontweight='bold', fontsize=11)
#
#bp1 = ax.boxplot(np.log10(np.vstack([hybrid_gen_RMSEs])).T, widths=0.5, medianprops = {'color':c1}, positions=[0])
#bp2 = ax.boxplot(np.log10(np.vstack([rnn_gen_RMSEs])).T, widths=0.5, medianprops = {'color':c2}, positions=[1])
#bp3 = ax.boxplot(np.log10(np.vstack([scipy_gen_RMSEs])).T, widths=0.5, medianprops = {'color':c4}, positions=[2])
#
#
#labels = [item.get_text() for item in ax.get_xticklabels()]
#labels[0] = 'ODE+RNN'
#labels[1] = 'sRNN'
#labels[2] = 'L-BFGS-B'
#
#ax.set_xticklabels(labels)
#
#ax.set_xlabel('Model')
#
#ax.tick_params(axis='y', labelsize=8)
#ax.tick_params(axis='x', labelsize=8)
#ax.set_ylim([-1.1, 1.1])







plt.show()

#plt.savefig('RW_Figure_1.png', dpi=300)
#plt.savefig('RW_Figure_2.png', dpi=300)
#plt.savefig('RW_Figure_3.png', dpi=300)
#plt.savefig('RW_Figure_4.png', dpi=300)

