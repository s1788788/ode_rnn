from scipy.integrate import solve_ivp
import numpy as np
from matplotlib import pyplot as plt
import torch
import math

from torch.autograd import Function
from torch.distributions.uniform import Uniform
from torch import nn

from codebase_bartol_parallel import *
from ODE_code_bartol import *

import time

#torch.manual_seed(10)
#np.random.seed(10)


###################################
######### INDICATOR CONCS ######### 
###################################

def return_init(which, conc):

    if which == 'Fluo4' and conc == '20':

        Ca_0        =   0.1  ##uM
        Fluo4_0     =  15.2  ##uM
        CaFluo4_0   =   4.8  ##uM
        OGB1_0      =   0.0  ##uM
        CaOGB1_0    =   0.0  ##uM
        IMCBP_0     =  75.5  ##uM
        CaIMCBP_0   =   3.5  ##uM
        NCX_0       =   2.0  ##uM
        CaNCX_0     =   1.0  ##uM
        PMCA_0      =  18.4  ##uM
        CaPMCA_0    =   0.6  ##uM


    elif which == 'OGB1' and conc == '20':

        Ca_0        =   0.1  ##uM
        Fluo4_0     =   0.0  ##uM
        CaFluo4_0   =   0.0  ##uM
        OGB1_0      =  13.5  ##uM
        CaOGB1_0    =   6.5  ##uM
        IMCBP_0     =  75.5  ##uM
        CaIMCBP_0   =   3.5  ##uM
        NCX_0       =   2.0  ##uM
        CaNCX_0     =   1.0  ##uM
        PMCA_0      =  18.4  ##uM
        CaPMCA_0    =   0.6  ##uM


    elif which == 'OGB1' and conc == '50':

        Ca_0        =   0.1  ##uM
        Fluo4_0     =   0.0  ##uM
        CaFluo4_0   =   0.0  ##uM
        OGB1_0      =  33.8  ##uM
        CaOGB1_0    =  16.2  ##uM
        IMCBP_0     =  75.5  ##uM
        CaIMCBP_0   =   3.5  ##uM
        NCX_0       =   2.0  ##uM
        CaNCX_0     =   1.0  ##uM
        PMCA_0      =  18.4  ##uM
        CaPMCA_0    =   0.6  ##uM


    elif which == 'OGB1' and conc == '100':

        Ca_0        =   0.1  ##uM
        Fluo4_0     =   0.0  ##uM
        CaFluo4_0   =   0.0  ##uM
        OGB1_0      =  67.6  ##uM
        CaOGB1_0    =  32.4  ##uM
        IMCBP_0     =  75.5  ##uM
        CaIMCBP_0   =   3.5  ##uM
        NCX_0       =   2.0  ##uM
        CaNCX_0     =   1.0  ##uM
        PMCA_0      =  18.4  ##uM
        CaPMCA_0    =   0.6  ##uM


    c_0     = np.array([Ca_0, Fluo4_0, CaFluo4_0, OGB1_0, CaOGB1_0, IMCBP_0, CaIMCBP_0, NCX_0, CaNCX_0, PMCA_0, CaPMCA_0])
    c_0_rnn = np.array([Ca_0, Fluo4_0, CaFluo4_0, OGB1_0, CaOGB1_0, IMCBP_0, CaIMCBP_0, 0])

    return c_0, c_0_rnn


##################################
#### LOADING/SAVING FUNCTIONS ####
##################################

def save_data(T, ODE_INIT, RNN_INIT, INS, OUTS, RNN_FLUO, name):
    
    with open('{}_t.npy'.format(name), 'wb') as f:
        np.save(f, T)

    with open('{}_ode_inits.npy'.format(name), 'wb') as f:
        np.save(f, ODE_INIT)

    with open('{}_rnn_inits.npy'.format(name), 'wb') as f:
        np.save(f, RNN_INIT)

    with open('{}_ins.npy'.format(name), 'wb') as f:
        np.save(f, INS)
    
    with open('{}_outs.npy'.format(name), 'wb') as f:
        np.save(f, OUTS)

    with open('{}_rnn_fluo.npy'.format(name), 'wb') as f:
        np.save(f, RNN_FLUO)

    return 0



def load_data(name):

    with open('{}_t.npy'.format(name), 'rb') as f:
        t = np.load(f)

    with open('{}_ode_inits.npy'.format(name), 'rb') as f:
        ode_inits = np.load(f)

    with open('{}_rnn_inits.npy'.format(name), 'rb') as f:
        rnn_inits = np.load(f)

    with open('{}_ins.npy'.format(name), 'rb') as f:
        ins = np.load(f)

    with open('{}_outs.npy'.format(name), 'rb') as f:
        outs = np.load(f)

    with open('{}_rnn_fluo.npy'.format(name), 'rb') as f:
        rnn_fluo = np.load(f)

    return t, ode_inits, rnn_inits, ins, outs, rnn_fluo

##################################
#### LOADING/SAVING FUNCTIONS ####
##################################


c_scale = 75.5 
t_scale = 0.01
dt = 0.01/t_scale
sim_time = 100/t_scale


rates = {'k1f' : 0.8   * t_scale, 'k1b' : 0.24  * t_scale, 
         'k2f' : 0.8   * t_scale, 'k2b' : 0.16  * t_scale,
         'k3f' : 0.247 * t_scale, 'k3b' : 0.524 * t_scale,
         'k4f' : 0.15  * t_scale, 'k4b' : 0.015 * t_scale, 'k4p' : 0.012 * t_scale, 'k4l' : 0.0043 * t_scale,
         'k5f' : 0.3   * t_scale, 'k5b' : 0.3   * t_scale, 'k5p' : 0.6   * t_scale, 'k5l' : 0.0194 * t_scale}

        

#################################
##### RANGE DATA SIMULATION #####
#################################
#rates_unscaled = {'k1f' : 0.8    , 'k1b' : 0.24   , 
#         'k2f' : 0.8    , 'k2b' : 0.16   ,
#         'k3f' : 0.247  , 'k3b' : 0.524  ,
#         'k4f' : 0.15   , 'k4b' : 0.015  , 'k4p' : 0.012  , 'k4l' : 0.0043  ,
#         'k5f' : 0.3    , 'k5b' : 0.3    , 'k5p' : 0.6    , 'k5l' : 0.0194  }
#
#c_0_noneq = np.array([0.1, 0, 0, 0, 0, 79, 0, 3, 0, 19, 0])
#
#dp = {'k_vals_scaled' : rates, 'k_vals_unscaled' : rates_unscaled, 't_max' : sim_time, 'dt' : dt, 'c_scale' : c_scale, 't_scale' : t_scale}
#
#stuff = gen_range_with_pre_sim(system = f_bartol, conc_min = 5.0, conc_max = 500.0, conc_step = 5.0, which_buffer = 'Fluo4', n_sims = 30, c_norm = c_scale, c_0_pre_sim = c_0_noneq, dp = dp, n_cores = 15)
#
#save_data(*stuff,  name='./Data/range_Fluo4')


#################################
##### RANGE DATA SIMULATION #####
#################################


#num_seen   = 8
#num_bridge = 1
#num_unseen = 100
#
#
#num_Lin_layers = 2
#num_Lin_NH = num_seen + num_bridge + num_unseen 
#
#dev = torch.device('cpu')
#
#
#params = {'n_seen' : num_seen, 'n_bridge' : num_bridge, 'n_unseen' : num_unseen, 'N_Lin_Layers' : num_Lin_layers, 'N_Lin_NH' : num_Lin_NH, 'dev' : dev, 'dt' : dt, 't_scale' : t_scale, 'c_scale' : c_scale}
#
#
#rnn_model = hybrid_object(p=params)
#####rnn_model.load_net(f_name = 'stuff_trained.pt')
##
##
##
#N_TRAIN_EPS = 10
#N_TRAIN_BATCHES = 100
#N_TRAIN_BATCH_SIZE = 30
#
#VAL_DATA_SIZE = 100
#TEST_DATA_SIZE = 100
#
#N_CORES = 15



#t, train_ode_inits, train_rnn_inits, train_ins, train_outs, train_rnn_fluo_init          = gen_toy_delta_perturb(system = f_bartol, k_vals = rates, y_init_func = return_init, t_max = sim_time, dt = dt, norm_conc = c_scale,  n_delta_low = 0, n_delta_high = 10, c_delta_low = 0.1, c_delta_high = 10, t_delta_low = 15/t_scale, t_delta_high = 85/t_scale, num_runs = N_TRAIN_BATCHES * N_TRAIN_BATCH_SIZE, n_cores = N_CORES)


#val_t, val_ode_inits, val_rnn_inits, val_ins, val_outs, val_rnn_fluo_init                = gen_toy_delta_perturb(system = f_bartol, k_vals = rates, y_init_func = return_init, t_max = sim_time, dt = dt, norm_conc = c_scale,  n_delta_low = 0, n_delta_high = 10, c_delta_low = 0.1, c_delta_high = 10, t_delta_low = 15/t_scale, t_delta_high = 85/t_scale, num_runs = 10000, n_cores = N_CORES)

#test_t, test_ode_inits, test_rnn_inits, test_ins, test_outs, test_rnn_fluo_init          = gen_toy_delta_perturb(system = f_bartol, k_vals = rates, y_init_func = return_init, t_max = sim_time, dt = dt, norm_conc = c_scale,  n_delta_low = 0, n_delta_high = 10, c_delta_low = 0.1, c_delta_high = 10, t_delta_low = 15/t_scale, t_delta_high = 85/t_scale, num_runs = TEST_DATA_SIZE, n_cores = N_CORES)
#
#gen_t, gen_ode_inits, gen_rnn_inits, gen_ins, gen_outs, gen_rnn_fluo_init                = gen_toy_exp_perturb(system = f_bartol, k_vals = rates, y_init_func = return_init, t_max = sim_time, dt = dt, norm_conc = c_scale, v2_r = [0.01, 0.05], td1_r = [0, 50/t_scale], td2_r = 20/t_scale, tau1_r = [10/t_scale, 20/t_scale], tau2_r = [10/t_scale, 20/t_scale], num_runs = TEST_DATA_SIZE, n_cores = N_CORES)
#
#
#
#save_data(t, train_ode_inits, train_rnn_inits, train_ins, train_outs, train_rnn_fluo_init,  name='./Data/train')
#save_data(val_t, val_ode_inits, val_rnn_inits, val_ins, val_outs, val_rnn_fluo_init,        name='./Data/RMSE_val')
#save_data(test_t, test_ode_inits, test_rnn_inits, test_ins, test_outs, test_rnn_fluo_init,  name='./Data/test')
#save_data(gen_t, gen_ode_inits, gen_rnn_inits, gen_ins, gen_outs, gen_rnn_fluo_init,        name='./Data/gen')



#t, train_ode_inits, train_rnn_inits, train_ins, train_outs, train_rnn_fluo_init = load_data('./Data/train')
#val_t, val_ode_inits, val_rnn_inits, val_ins, val_outs, val_rnn_fluo_init       = load_data('./Data/val')
#test_t, test_ode_inits, test_rnn_inits, test_ins, test_outs, test_rnn_fluo_init = load_data('./Data/test')
#gen_t, gen_ode_inits, gen_rnn_inits, gen_ins, gen_outs, gen_rnn_fluo_init       = load_data('./Data/gen')




#l_rate = 0.01
#w_lam  = 0.0000
#
#sch_step_size = 50
#sch_gamma     = 0.7
#
#opt = torch.optim.Adam(rnn_model.net.parameters(), lr = l_rate, weight_decay = w_lam)
#sch = torch.optim.lr_scheduler.StepLR(opt, step_size = 50, gamma = 0.5)


##hybrid_train_ep_loss, hybrid_train_batch_loss, hybrid_train_indiv_loss, hybrid_val_ep_loss, hybrid_val_batch_loss, hybrid_val_indiv_loss, hybrid_optimal_train_losses, hybrid_optimal_val_losses = rnn_model.train_model(train_in = train_ins, train_out =  train_outs, t=t, train_c_0 = train_rnn_inits, N_EPS = N_TRAIN_EPS, N_BATCHES = N_TRAIN_BATCHES, BATCH_SIZE = N_TRAIN_BATCH_SIZE, opt = opt, sch = sch, val_in = val_ins, val_out = val_outs, val_c_0 = val_rnn_inits, n_cores = N_CORES, early_stop = True)
#

#net_name = 'hybrid_model_NLAYER_{}_NH_{}_NEPS_{}_NBATCHES_{}_BATCHSIZE_{}_LR_{}_WLAM_{}_SCHSTEP_{}_SCHGAMMA_{}'.format(num_Lin_layers, num_unseen, N_TRAIN_EPS, N_TRAIN_BATCHES, N_TRAIN_BATCH_SIZE, l_rate, w_lam, sch_step_size, sch_gamma)
#print(net_name)


#hybrid_train_ep_loss, hybrid_train_batch_loss, hybrid_train_indiv_loss, _, _, _, _, _ = rnn_model.train_model(train_in = train_ins, train_out =  train_outs, t=t, train_c_0 = train_rnn_inits, N_EPS = N_TRAIN_EPS, N_BATCHES = N_TRAIN_BATCHES, BATCH_SIZE = N_TRAIN_BATCH_SIZE, opt = opt, sch = sch, val_in = None, val_out = None, val_c_0 = None, n_cores = N_CORES, early_stop = False)
#
#rnn_model.save_net('./Models/hybrid_model_NLAYER_{}_NH_{}_NEPS_{}_NBATCHES_{}_BATCHSIZE_{}_LR_{}_WLAM_{}_SCHSTEP_{}_SCHGAMMA_{}.pt'.format(num_Lin_layers, num_unseen, N_TRAIN_EPS, N_TRAIN_BATCHES, N_TRAIN_BATCH_SIZE, l_rate, w_lam, sch_step_size, sch_gamma))




#net_name = 'hybrid_model_NLAYER_{}_NH_{}_NEPS_{}_NBATCHES_{}_BATCHSIZE_{}_LR_{}_WLAM_{}_SCHSTEP_{}_SCHGAMMA_{}'.format(num_Lin_layers, num_unseen, N_TRAIN_EPS, N_TRAIN_BATCHES, N_TRAIN_BATCH_SIZE, l_rate, w_lam, sch_step_size, sch_gamma)
#
#rnn_model.load_net('./Models/' + net_name + '.pt')



#hybrid_train_ep_loss, hybrid_train_batch_loss, hybrid_train_indiv_loss, _, _, _, _, _ = rnn_model.return_losses()
#
#plt.subplot(1, 3, 1)
#plt.title('INDIVIDUAL LOSSES')
#plt.plot((np.sqrt(np.hstack(hybrid_train_indiv_loss))), color='black')
#
#plt.subplot(1, 3, 2)
#plt.title('BATCH LOSSES')
#plt.plot(np.hstack(hybrid_train_batch_loss), color='black')
#
#plt.subplot(1, 3, 3)
#plt.title('EP LOSS')
#plt.plot(np.sqrt(np.array(hybrid_train_ep_loss))/30, color='black', label='Training')
#
#with open('./RMSEs/' + net_name + '_RMSE_val.npy', 'rb') as f:
#    RMSE_vals = np.load(f)
#
#plt.plot(np.mean(RMSE_vals), 'x', color='orange', label='Validation')
#plt.legend()
#
#plt.show()




#################################
#### HYBRID MODEL VALIDATION ####
#################################
#N_TRAIN_EPS = 4
#N_TRAIN_BATCHES = 100
#N_TRAIN_BATCH_SIZE = 30
#
###VAL_DATA_SIZE = 100
###TEST_DATA_SIZE = 100
#
#N_CORES = 15
#
#l_rate = 0.01
#w_lam  = 0.0000
#
#sch_step_size = 30
#sch_gamma     = 0.22
#
#net_name = 'hybrid_model_NLAYER_{}_NH_{}_NEPS_{}_NBATCHES_{}_BATCHSIZE_{}_LR_{}_WLAM_{}_SCHSTEP_{}_SCHGAMMA_{}'.format(num_Lin_layers, num_unseen, N_TRAIN_EPS, N_TRAIN_BATCHES, N_TRAIN_BATCH_SIZE, l_rate, w_lam, sch_step_size, sch_gamma)

#rnn_model.load_net('./Models/' + net_name + '.pt')


##### VAL DATA ####
#print(net_name)
#RMSE_t, RMSE_ode_inits, RMSE_rnn_inits, RMSE_ins, RMSE_outs, RMSE_rnn_fluo_init       = load_data('./Data/RMSE_val')
#RMSE_dat = {'t' : RMSE_t, 'ins' : RMSE_ins, 'outs' : RMSE_outs, 'rnn_inits' : RMSE_rnn_inits}
#hybrid_model_validation(model = rnn_model, data = RMSE_dat, num_runs = 50, sample_size = 300, name = net_name, prefix = 'RMSE_val', n_cores = 15, verbose = False)


##### TEST DATA ####
#RMSE_t, RMSE_ode_inits, RMSE_rnn_inits, RMSE_ins, RMSE_outs, RMSE_rnn_fluo_init       = load_data('./Data/RMSE_test')
#RMSE_dat = {'t' : RMSE_t, 'ins' : RMSE_ins, 'outs' : RMSE_outs, 'rnn_inits' : RMSE_rnn_inits}
#hybrid_model_validation(model = rnn_model, data = RMSE_dat, num_runs = 50, sample_size = 300, name = net_name, prefix = 'RMSE_test', n_cores = 15, verbose = False)



##### GEN DATA ####
#RMSE_t, RMSE_ode_inits, RMSE_rnn_inits, RMSE_ins, RMSE_outs, RMSE_rnn_fluo_init       = load_data('./Data/RMSE_gen')
#RMSE_dat = {'t' : RMSE_t, 'ins' : RMSE_ins, 'outs' : RMSE_outs, 'rnn_inits' : RMSE_rnn_inits}
#hybrid_model_validation(model = rnn_model, data = RMSE_dat, num_runs = 50, sample_size = 300, name = net_name, prefix = 'RMSE_gen', n_cores = 15, verbose = False)
#
#
########################
###### RANGE DATA 1 ####
########################
#
#RMSE_t, RMSE_ode_inits, RMSE_rnn_inits, RMSE_ins, RMSE_outs, RMSE_rnn_fluo_init       = load_data('./Data/range_OGB1')
#range_loss, range_loss_individual, range_out = rnn_model.eval_inputs(ins = RMSE_ins, outs =  RMSE_outs, t=RMSE_t, y_0 = RMSE_rnn_inits, n_cores = N_CORES, verb=True)
#
#with open('./RMSEs/' + net_name + '_' + 'range_OGB1.npy', 'wb') as f:
#    np.save(f, range_loss_individual)


#######################
##### RANGE DATA 2 ####
#######################

#RMSE_t, RMSE_ode_inits, RMSE_rnn_inits, RMSE_ins, RMSE_outs, RMSE_rnn_fluo_init       = load_data('./Data/range_Fluo4')
#range_loss, range_loss_individual, range_out = rnn_model.eval_inputs(ins = RMSE_ins, outs =  RMSE_outs, t=RMSE_t, y_0 = RMSE_rnn_inits, n_cores = N_CORES, verb=True)
#
#with open('./RMSEs/' + net_name + '_' + 'range_Fluo4.npy', 'wb') as f:
#    np.save(f, range_loss_individual)



#################################
#### HYBRID MODEL VALIDATION ####
#################################


#c_scale = 75.5 
#t_scale = 0.01
#dt = 0.01/t_scale
#sim_time = 100/t_scale




#t, test_ode_inits1, test_rnn_inits1, test_ins1, test_outs1, rnn_fluo_stuff1 = gen_toy_delta_perturb(system = f_bartol, k_vals = rates, y_init_func = return_init, t_max = 1000/t_scale, dt = dt, norm_conc = c_scale,  n_delta_low = 0, n_delta_high = 10, c_delta_low = 0.5, c_delta_high = 5, t_delta_low = 15/t_scale, t_delta_high = 75/t_scale, num_runs = 1, n_cores = N_CORES)
#
#
##val_t, test_ode_inits2, test_rnn_inits2, test_ins2, test_outs2 = gen_toy_exp_perturb(system = f_bartol, k_vals = rates, y_init_func = return_init, t_max = 1000/t_scale, dt = dt, norm_conc = c_scale, v2_r = [0.01, 0.05], td1_r = [0, 50/t_scale], td2_r = 20/t_scale, tau1_r = [10/t_scale, 20/t_scale], tau2_r = [10/t_scale, 20/t_scale], num_runs = 1, n_cores = N_CORES)
#
#
#
##stuff = torch.load('stuff_trained.pt')
###print(stuff.keys())
##
####plt.plot(np.sqrt(np.array(stuff['train_batch_loss'][0])/40), linewidth=3, alpha=0.7, label='Training')
####plt.plot(np.sqrt(np.array(stuff['val_batch_loss'][0])/50), linewidth=3, alpha=0.7, label='Validation')
##plt.plot((np.sqrt(np.array(stuff['train_batch_loss'][0])/30)), linewidth=3, alpha=0.7, label='Training')
##plt.plot((np.sqrt(np.array(stuff['val_batch_loss'][0])/30)), linewidth=3, alpha=0.7, label='Validation')
##
##plt.xlabel('Batch', fontsize=14)
##plt.ylabel('RMSE' , fontsize=14)
##plt.legend(prop={'size':16}, ncol=2)
##plt.show()
#
#
#
#test_loss1, test_rnn1 = rnn_model.eval_inputs(ins = test_ins1, outs =  test_outs1, t=t, y_0 = test_rnn_inits1, n_cores = N_CORES)
##test_loss2, test_rnn2 = rnn_model.eval_inputs(ins = test_ins2, outs =  test_outs2, t=t, y_0 = test_rnn_inits2, n_cores = N_CORES)
#
#
#
#plt.plot(t * t_scale, c_scale * test_outs1[0][2, :], label='True CaFluo4', linewidth=3, alpha=0.7)
#plt.plot(t * t_scale, c_scale * test_rnn1[0][2,:]  , label='RNN  CaFluo4', linewidth=3, alpha=0.7)
#
#plt.plot(t * t_scale, c_scale * test_outs1[0][4, :], label='True CaOGB1', linewidth=3, alpha=0.7)
#plt.plot(t * t_scale, c_scale * test_rnn1[0][4,:]  , label='RNN  CaOGB1', linewidth=3, alpha=0.7)
#
#plt.xlabel('Time (ms)', fontsize=14)
#plt.ylabel('Conc. (uM)', fontsize=14)
#
#plt.title('100uM OGB1', fontsize=16)
#
#plt.legend(prop={'size':14})
#plt.show()





#print(ins[0][0,:].shape)
#print(outs[0].shape)
#
#plt.plot(t, ins[0][0,:])
#plt.plot(t, test_outs[0].squeeze(), label='RNN')
#plt.plot(t, outs[0], label='ODE')
#plt.legend()
#plt.show()



##
##plt.subplot(1, 2, 2)
##plt.title('Exponential')
#plt.plot(t*t_scale, test_outs[0].squeeze(), label='RNN', linewidth=3, alpha=0.7)
#plt.plot(t*t_scale, outs[0].squeeze(),   label='True ODE', linewidth=3, alpha=0.7)
#plt.xlabel('Time (ms)', fontsize=16)
#plt.ylabel('Norm. conc', fontsize=16)
#plt.legend()
##
##plt.subplot(1, 2, 1)
#plt.title('Pulses')
#plt.plot(t*t_scale, test_outs2[0].squeeze(), label='RNN', linewidth=3, alpha=0.7)
#plt.plot(t*t_scale, val_outs2[0].squeeze(),  label='True ODE', linewidth=3, alpha=0.7)
#plt.xlabel('Time (ms)', fontsize=16)
#plt.ylabel('Norm. conc', fontsize=16)
#plt.legend()
#
#plt.show()




######################################################
################## RNN BASELINE ######################
######################################################


#t, train_ode_inits, train_rnn_inits, train_ins, train_outs, train_rnn_fluo_init = load_data('./Data/train')
#
#train_ins  = [np.vstack([train_ins[i][0,:], np.tile(train_rnn_fluo_init[i][:,None], len(t))]) for i in range(len(train_ins))]    
#train_outs = [np.vstack([train_outs[i][2,:], train_outs[i][4,:]]) for i in range(len(train_outs))]
#
#
baseline_p = {'d_in' : 3, 'd_h' : 100, 'd_out' : 2, 'N_LAYER' : 2, 'N_NH' : 100, 'nonlin' : 'Tanh'}

rnn_baseline = rnn_baseline_object(p = baseline_p)

N_TRAIN_EPS = 10
N_TRAIN_BATCHES = 100
N_TRAIN_BATCH_SIZE = 30

#####VAL_DATA_SIZE = 100
#####TEST_DATA_SIZE = 100

N_CORES = 15


l_rate = 0.001
w_lam  = 0.0000000

sch_step_size = 50
sch_gamma = 0.8

opt = torch.optim.Adam(rnn_baseline.net.parameters(), lr = l_rate, weight_decay = w_lam)
sch = torch.optim.lr_scheduler.StepLR(opt, step_size = sch_step_size, gamma = sch_gamma)

net_name = 'RNN_baseline_model_NLAYER_{}_NH_{}_NEPS_{}_NBATCHES_{}_BATCHSIZE_{}_LR_{}_WLAM_{}_SCHSTEP_{}_SCHGAMMA_{}'.format(baseline_p['N_LAYER'], baseline_p['N_NH'], N_TRAIN_EPS, N_TRAIN_BATCHES, N_TRAIN_BATCH_SIZE, l_rate, w_lam, sch_step_size, sch_gamma)
print(net_name)


#rnn_train_losses, rnn_train_batch_losses, rnn_train_all_losses, rnn_val_losses, rnn_val_batch_losses, rnn_val_all_losses = rnn_baseline.train_model(train_in = train_ins, train_out = train_outs, t = t, N_EPS = N_TRAIN_EPS, N_BATCHES = N_TRAIN_BATCHES, BATCH_SIZE = N_TRAIN_BATCH_SIZE, opt=opt, sch=sch, n_cores = 15, val_in = None, val_out = None, early_stop=True)
#
#rnn_baseline.save_net(save_name = './Models/' + net_name + '.pt')


##########################
##### RNN VALIDATION #####
##########################

rnn_baseline.load_net(f_name='./Models/'+net_name + '.pt')
bline_train_ep_loss, bline_train_batch_loss, bline_train_indiv_loss, _, _, _ = rnn_baseline.return_losses()


#print(len(bline_train_ep_loss[0]))
#print(len(bline_train_batch_loss[0]))
#ep_loss = [np.sqrt(np.mean(np.hstack(bline_train_indiv_loss)[3000 * j: 3000 * (j+1)])) for j in range(10)]
#
#plt.subplot(1, 3, 1)
#plt.title('INDIVIDUAL LOSSES')
#plt.plot((np.sqrt(np.hstack(bline_train_indiv_loss))), color='black')
#
#plt.subplot(1, 3, 2)
#plt.title('BATCH LOSSES')
#plt.plot(np.hstack(bline_train_batch_loss), color='black')
#
#plt.subplot(1, 3, 3)
#plt.title('EP LOSS')
#plt.plot((np.sqrt(np.array(bline_train_ep_loss)/30)), '-x', color='black')
#
#plt.show()





#RMSE_t, RMSE_ode_inits, RMSE_rnn_inits, RMSE_ins, RMSE_outs, RMSE_rnn_fluo_init = load_data('./Data/RMSE_val')
#RMSE_ins  = [np.vstack([RMSE_ins[i][0,:], np.tile(RMSE_rnn_fluo_init[i][:,None], len(RMSE_t))]) for i in range(len(RMSE_ins))]    
#RMSE_outs = [np.vstack([RMSE_outs[i][2,:], RMSE_outs[i][4,:]]) for i in range(len(RMSE_outs))]
#data = {'t':RMSE_t, 'ins':RMSE_ins, 'outs':RMSE_outs}
#
#RNN_val_RMSEs = RNN_model_validation(model = rnn_baseline, data = data, num_runs = 50, sample_size = 300, name = net_name, prefix = 'RMSE_val', n_cores = N_CORES, verbose=False)



#RMSE_t, RMSE_ode_inits, RMSE_rnn_inits, RMSE_ins, RMSE_outs, RMSE_rnn_fluo_init = load_data('./Data/RMSE_test')
#RMSE_ins  = [np.vstack([RMSE_ins[i][0,:], np.tile(RMSE_rnn_fluo_init[i][:,None], len(RMSE_t))]) for i in range(len(RMSE_ins))]    
#RMSE_outs = [np.vstack([RMSE_outs[i][2,:], RMSE_outs[i][4,:]]) for i in range(len(RMSE_outs))]
#data = {'t':RMSE_t, 'ins':RMSE_ins, 'outs':RMSE_outs}
#
#RNN_val_RMSEs = RNN_model_validation(model = rnn_baseline, data = data, num_runs = 50, sample_size = 300, name = net_name, prefix = 'RMSE_test', n_cores = N_CORES, verbose=False)



RMSE_t, RMSE_ode_inits, RMSE_rnn_inits, RMSE_ins, RMSE_outs, RMSE_rnn_fluo_init = load_data('./Data/RMSE_gen')
RMSE_ins  = [np.vstack([RMSE_ins[i][0,:], np.tile(RMSE_rnn_fluo_init[i][:,None], len(RMSE_t))]) for i in range(len(RMSE_ins))]    
RMSE_outs = [np.vstack([RMSE_outs[i][2,:], RMSE_outs[i][4,:]]) for i in range(len(RMSE_outs))]
data = {'t':RMSE_t, 'ins':RMSE_ins, 'outs':RMSE_outs}

RNN_val_RMSEs = RNN_model_validation(model = rnn_baseline, data = data, num_runs = 50, sample_size = 300, name = net_name, prefix = 'RMSE_gen', n_cores = N_CORES, verbose=False)




#######################
##### RANGE DATA 1 ####
#######################
RMSE_t, RMSE_ode_inits, RMSE_rnn_inits, RMSE_ins, RMSE_outs, RMSE_rnn_fluo_init       = load_data('./Data/range_OGB1')

RMSE_ins  = [np.vstack([RMSE_ins[i][0,:], np.tile(RMSE_rnn_fluo_init[i][:,None], len(RMSE_t))]) for i in range(len(RMSE_ins))]    
RMSE_outs = [np.vstack([RMSE_outs[i][2,:], RMSE_outs[i][4,:]]) for i in range(len(RMSE_outs))]

range_loss, range_loss_individual, range_out = rnn_baseline.eval_inputs(ins = RMSE_ins, outs =  RMSE_outs, t=RMSE_t, n_cores = N_CORES, verb=True)

with open('./RMSEs/' + net_name + '_' + 'range_OGB1.npy', 'wb') as f:
    np.save(f, range_loss_individual)



#######################
##### RANGE DATA 2 ####
#######################
RMSE_t, RMSE_ode_inits, RMSE_rnn_inits, RMSE_ins, RMSE_outs, RMSE_rnn_fluo_init       = load_data('./Data/range_Fluo4')

RMSE_ins  = [np.vstack([RMSE_ins[i][0,:], np.tile(RMSE_rnn_fluo_init[i][:,None], len(RMSE_t))]) for i in range(len(RMSE_ins))]    
RMSE_outs = [np.vstack([RMSE_outs[i][2,:], RMSE_outs[i][4,:]]) for i in range(len(RMSE_outs))]

range_loss, range_loss_individual, range_out = rnn_baseline.eval_inputs(ins = RMSE_ins, outs =  RMSE_outs, t=RMSE_t, n_cores = N_CORES, verb=True)

with open('./RMSEs/' + net_name + '_' + 'range_Fluo4.npy', 'wb') as f:
    np.save(f, range_loss_individual)

##########################
##### RNN VALIDATION #####
##########################



#RMSE_t, RMSE_ode_inits, RMSE_rnn_inits, RMSE_ins, RMSE_outs, RMSE_rnn_fluo_init          = gen_toy_delta_perturb(system = f_bartol, k_vals = rates, y_init_func = return_init, t_max = sim_time, dt = dt, norm_conc = c_scale,  n_delta_low = 0, n_delta_high = 10, c_delta_low = 0.1, c_delta_high = 20, t_delta_low = 15/t_scale, t_delta_high = 85/t_scale, num_runs = 1, n_cores = N_CORES)
#
#
#
#h_loss, h_loss_individual, h_out = rnn_model.eval_inputs(ins = RMSE_ins, outs =  RMSE_outs, t=RMSE_t, y_0 = RMSE_rnn_inits, n_cores = N_CORES, verb=True)
#
#RMSE_ins  = [np.vstack([RMSE_ins[i][0,:], np.tile(RMSE_rnn_fluo_init[i][:,None], len(RMSE_t))]) for i in range(len(RMSE_ins))]    
#RMSE_outs = [np.vstack([RMSE_outs[i][2,:], RMSE_outs[i][4,:]]) for i in range(len(RMSE_outs))]
#
#r_loss, r_loss_individual, r_out = rnn_baseline.eval_inputs(ins = RMSE_ins, outs =  RMSE_outs, t=RMSE_t, n_cores = N_CORES, verb=True)
#
##print(h_out.shape)
#
#
#plt.plot(RMSE_t, RMSE_outs[0][0,:], label='ODE Fluo4', alpha=0.7)
#plt.plot(RMSE_t, h_out[0][2,:], label='Hybrid Fluo4', alpha=0.7)
#plt.plot(RMSE_t, r_out[0][0,:], label='RNN Fluo4', alpha=0.7)
#
#
#
#plt.plot(RMSE_t, RMSE_outs[0][1,:], label='ODE OGB1', alpha=0.7)
#plt.plot(RMSE_t, h_out[0][4,:], label='Hybrid OGB1', alpha=0.7)
#plt.plot(RMSE_t, r_out[0][1,:], label='RNN OGB1', alpha=0.7)
#
#plt.legend()
#plt.show()
#
#stuff = np.vstack([RMSE_t, RMSE_outs[0][0,:], h_out[0][2,:], r_out[0][0,:], RMSE_outs[0][1,:], h_out[0][4,:], r_out[0][1,:]])
#
#
#with open('plot_stuff.npy', 'wb') as f:
#    np.save(f, stuff)




#hybrid_loss, hybrid_indiv_loss, hybrid_out = rnn_model.eval_inputs(ins = val_ins[:1], outs =  val_outs[:1], t=val_t, y_0 = val_rnn_inits[:1], n_cores = 15, verb=False)
#
#val_ins  = [np.vstack([val_ins[i][0,:], np.tile(val_rnn_fluo_init[i][:,None], len(val_t))]) for i in range(len(val_ins))]    
#val_outs = [np.vstack([val_outs[i][2,:], val_outs[i][4,:]]) for i in range(len(val_outs))]
#
#r_loss, r_loss_individual, r_out = rnn_baseline.eval_inputs(ins = val_ins[:1], outs =  val_outs[:1], t=val_t, n_cores = 15, verb=True)
#
#plt.plot(val_outs[0][1,:])
#plt.plot(hybrid_out[0][4,:])
#plt.plot(r_out[0][1,:])
#
#plt.show()
#
#res = np.vstack([val_outs[0][1,:], hybrid_out[0][4,:], r_out[0][1,:]])
#
#print(res.shape)
#
#with open('./plot_stuff2.npy', 'wb') as f:
#
#    np.save(f, res)







#plt.subplot(1, 3, 1)
#plt.title('INDIVIDUAL LOSSES')
#plt.plot(np.hstack(rnn_train_all_losses), color='black')
#plt.plot(np.hstack(rnn_val_all_losses),   color='red')
#
#plt.subplot(1, 3, 2)
#plt.title('BATCH LOSSES')
#plt.plot(np.hstack(rnn_train_batch_losses), color='black')
#plt.plot(np.hstack(rnn_val_batch_losses),   color='red')
#
#plt.subplot(1, 3, 3)
#plt.title('EP LOSS')
#plt.plot(rnn_train_losses, color='black')
#plt.plot(rnn_val_losses,   color='red')
#
#plt.show()
###plt.savefig('training_loss.png', dpi=180)
#
#
#
#
#
##rnn_baseline.load_net(f_name = 'RNN_baseline.pt')
#
#
#
#t, test_ode_inits, test_rnn_inits, test_ins, test_outs, test_rnn_fluo_stuff = gen_toy_delta_perturb(system = f_bartol, k_vals = rates, y_init_func = return_init, t_max = sim_time, dt = dt, norm_conc = c_scale,  n_delta_low = 0, n_delta_high = 10, c_delta_low = 0.5, c_delta_high = 5, t_delta_low = 15/t_scale, t_delta_high = 75/t_scale, num_runs = 1, n_cores = 15)
#
#test_ins  = [np.vstack([test_ins[i][0,:], np.tile(test_rnn_fluo_stuff[i][:,None], len(t))]) for i in range(len(test_ins))]    
#test_outs = [np.vstack([test_outs[i][2,:], test_outs[i][4,:]]) for i in range(len(test_outs))]
#
#test_loss_full, test_individual_losses, rnn_test_outs = rnn_baseline.eval_inputs(ins = test_ins, outs = test_outs, t = t, n_cores = 15)
#
#
#plt.plot(t, test_outs[0][0,:],     label='ODE CaFluo4')
#plt.plot(t, rnn_test_outs[0][0,:], label='RNN CaFluo4')
#
#plt.plot(t, test_outs[0][1,:],     label='ODE CaOGB1')
#plt.plot(t, rnn_test_outs[0][1,:], label='RNN CaOGB1')
#
#plt.title('Loss is {}'.format(test_individual_losses[0]), fontsize=16)
#
#plt.legend()
#plt.show()
##plt.savefig('test_loss.png', dpi=180)

