from scipy.integrate import solve_ivp
import numpy as np
import torch
import math

from torch.autograd import Function
from torch.distributions.uniform import Uniform
from torch import nn

from matplotlib import pyplot as plt

from joblib import Parallel, delayed

import time


class bartol_hybrid_1_pump(Function):
### ONLY ONE PUMP SPECIES WHERE ALL K_D, K_on, K_pump and K_leak are parameters

    
    @staticmethod
    def forward(ctx, inp, W, k, pi, time_step, t_scale, c_scale, dev):
    
        Ca      = inp[0]
        Fluo4   = inp[1]
        CaFluo4 = inp[2]
        OGB1    = inp[3]
        CaOGB1  = inp[4]
        IMCBP   = inp[5]
        CaIMCBP = inp[6]
        P       = inp[7]
        CaP     = inp[8]

        k1f = 0.8  * t_scale
        k1b = 0.24 * t_scale

        k2f = 0.8  * t_scale
        k2b = 0.16 * t_scale

        k3f = 0.247  * t_scale
        k3b = 0.524  * t_scale


        Kd   = torch.exp(k[0])
        on   = t_scale * 1E-2 * torch.exp(k[1]) 
        off  = Kd * on
        pump = t_scale * 1E-2 * torch.exp(k[2])
        leak = t_scale * 1E-2 * torch.exp(k[3])


        a1 = W.mm(inp)
        f = torch.tensor([
            [-k1f * Ca * Fluo4 * c_scale + k1b * CaFluo4 - k2f * Ca * OGB1 * c_scale + k2b * CaOGB1 - k3f * Ca * IMCBP * c_scale + k3b * CaIMCBP - on * Ca * P * c_scale + off * CaP + leak * P],
            [-k1f * Ca * Fluo4 * c_scale + k1b * CaFluo4],
            [ k1f * Ca * Fluo4 * c_scale - k1b * CaFluo4], 

            [-k2f * Ca * OGB1  * c_scale + k2b * CaOGB1 ],
            [ k2f * Ca * OGB1  * c_scale - k2b * CaOGB1 ], 

            [-k3f * Ca * IMCBP * c_scale + k3b * CaIMCBP],
            [ k3f * Ca * IMCBP * c_scale - k3b * CaIMCBP], 

            [  on * Ca * P * c_scale - off * CaP + pump * CaP], 
            [ -on * Ca * P * c_scale + off * CaP + pump * CaP ] ]).to(dev)
        b = torch.cat((f, pi))
        alpha = a1 + time_step * b

        ctx.save_for_backward(inp, W, k, pi, b, torch.tensor(time_step), torch.tensor(t_scale), torch.tensor(c_scale))

        return alpha


    @staticmethod
    def backward(ctx, grad_output):
        inp, W, k, pi, b, time_step, t_scale, c_scale = ctx.saved_tensors


        Ca      = inp[0]
        Fluo4   = inp[1]
        CaFluo4 = inp[2]
        OGB1    = inp[3]
        CaOGB1  = inp[4]
        IMCBP   = inp[5]
        CaIMCBP = inp[6]
        P       = inp[7]
        CaP     = inp[8]

        k1f = 0.8  * t_scale
        k1b = 0.24 * t_scale

        k2f = 0.8  * t_scale
        k2b = 0.16 * t_scale

        k3f = 0.247  * t_scale
        k3b = 0.524  * t_scale


        Kd    = torch.exp(k[0])
        k4f   = t_scale * 1E-2 * torch.exp(k[1]) 
        k4b   = Kd * k4f 
        k4p   = t_scale * 1E-2 * torch.exp(k[2])
        k4l   = t_scale * 1E-2 * torch.exp(k[3])



        #################################
        #################################
        

        B_h_M1 = torch.tensor([
            [-k1f * Fluo4 * c_scale - k2f * OGB1 * c_scale - k3f * IMCBP * c_scale - k4f * P * c_scale,      -k1f * Ca * c_scale,      k1b,        -k2f * Ca * c_scale,        k2b,        -k3f * Ca * c_scale,        k3b,        k4b,        -k4f * Ca * c_scale + k4l       ],
            ######################################################################
            [-k1f * Fluo4 * c_scale, - k1f * Ca * c_scale,  k1b, 0, 0, 0, 0, 0, 0],
            [ k1f * Fluo4 * c_scale,   k1f * Ca * c_scale, -k1b, 0, 0, 0, 0, 0, 0],
            ######################################################################
            [-k2f * OGB1  * c_scale, 0, 0, -k2f * Ca * c_scale,  k2b, 0, 0, 0, 0 ],
            [ k2f * OGB1  * c_scale, 0, 0,  k2f * Ca * c_scale, -k2b, 0, 0, 0, 0 ],
            ######################################################################
            [-k3f * IMCBP * c_scale, 0, 0, 0, 0, -k3f * Ca * c_scale,  k3b, 0 ,0 ],
            [ k3f * IMCBP * c_scale, 0, 0, 0, 0,  k3f * Ca * c_scale, -k3b, 0 ,0 ],
            ######################################################################
            [ k4f * P * c_scale, 0, 0, 0, 0, 0, 0,-k4b - k4p,  k4f * Ca * c_scale],
            [-k4f * P * c_scale, 0, 0, 0, 0, 0, 0, k4b + k4p, -k4f * Ca * c_scale]])

        B_h = torch.zeros((inp.shape[0], inp.shape[0]))
        B_h[:9, :9] = B_h_M1

        grad_inp  = (grad_output.t().mm(W + time_step * B_h)).t()


        #################################
        #################################


        grad_W  = torch.ger(inp.squeeze(), grad_output.squeeze())


        #################################
        #################################


        B_k_1 = torch.tensor([
            [Kd * k4f * CaP, -k4f * Ca * P * c_scale + Kd * k4f * CaP, 0, k4l * P],
            ###########################################
            [ 0, 0, 0, 0],
            [ 0, 0, 0, 0],
            ###########################################
            [ 0, 0, 0, 0],
            [ 0, 0, 0, 0],
            ###########################################
            [ 0, 0, 0, 0],
            [ 0, 0, 0, 0],
            ###########################################
            [-k4f * Kd * CaP,  k4f * Ca * P * c_scale - k4f * Kd * CaP, -k4f * CaP, 0],
            [ k4f * Kd * CaP, -k4f * Ca * P * c_scale + k4f * Kd * CaP,  k4f * CaP, 0]])

        
        B_k = torch.zeros((inp.shape[0], 4))
        B_k[:9,:] = B_k_1
        
        grad_k = (grad_output.t().mm(B_k)) * time_step
        

        #################################
        #################################


        pi_M1 = torch.eye(pi.shape[0])
        B_pi = torch.zeros((inp.shape[0], pi.shape[0]))
        B_pi[-pi.shape[0]:,:] = pi_M1

        
        grad_pi = (grad_output.t().mm(B_pi)).t() * time_step
        

        #################################
        #################################

        return grad_inp, grad_W, grad_k, grad_pi, None, None, None, None





#class toy_hybrid(Function):
#### BOTH K_D AND K_on are parameters
#
#    
#    @staticmethod
#    def forward(ctx, inp, W, k, pi, time_step, t_scale, c_scale, dev):
#    
#        k1f = 0.8  * t_scale
#        k1b = 0.16 * t_scale
#        Kd  = torch.exp(k[0])
#        on  = t_scale * 1E-1 * torch.exp(k[1]) 
#        off = Kd * on
#
#        a1 = W.mm(inp)
#        f = torch.tensor([
#            [-k1f * inp[0] * inp[1] * c_scale + k1b * inp[2] - on * inp[0] * inp[4] * c_scale + off * inp[3]],
#            [-k1f * inp[0] * inp[1] * c_scale + k1b * inp[2]],
#            [ k1f * inp[0] * inp[1] * c_scale - k1b * inp[2]], 
#            [  on * inp[0] * inp[4] * c_scale - off * inp[3]], 
#            [ -on * inp[0] * inp[4] * c_scale + off * inp[3]] ]).to(dev)
#        b = torch.cat((f, pi))
#        alpha = a1 + time_step * b
#
#        ctx.save_for_backward(inp, W, k, pi, b, torch.tensor(time_step), torch.tensor(t_scale), torch.tensor(c_scale))
#
#        return alpha
#
#
#    @staticmethod
#    def backward(ctx, grad_output):
#        inp, W, k, pi, b, time_step, t_scale, c_scale = ctx.saved_tensors
#
##        print(grad_output)
#
#        k1f = 0.8  * t_scale
#        k1b = 0.16 * t_scale
#        Kd  = torch.exp(k[0])
#        on  = t_scale * 1E-1 * torch.exp(k[1])
#        off = Kd * on
#
#        #################################
#        B_h_M1 = torch.tensor([
#            [-k1f * inp[1] * c_scale  - on  * inp[4] * c_scale, -k1f * inp[0] * c_scale, k1b, off, -on * inp[0] * c_scale],
#            [-k1f * inp[1] * c_scale, - k1f * inp[0] * c_scale,  k1b, 0, 0],
#            [ k1f * inp[1] * c_scale,   k1f * inp[0] * c_scale, -k1b, 0, 0],
#            [  on * inp[4] * c_scale, 0, 0, -off,  on * inp[0] * c_scale ],
#            [ -on * inp[4] * c_scale, 0, 0,  off, -on * inp[0] * c_scale ] ])
#
#        B_h = torch.zeros((inp.shape[0], inp.shape[0]))
#        B_h[:5, :5] = B_h_M1
#
#        grad_inp  = (grad_output.t().mm(W + time_step * B_h)).t()
#        #################################
#        #################################
#
#        grad_W  = torch.ger(inp.squeeze(), grad_output.squeeze())
#
#
#        #################################
#        #################################
#        B_k_1 = torch.tensor([
#            [ off * inp[3], -on * inp[0] * inp[4] * c_scale + off * inp[3]],
#            [0, 0],
#            [0, 0],
#            [-off * inp[3],  on * inp[0] * inp[4] * c_scale - off * inp[3]],
#            [ off * inp[3], -on * inp[0] * inp[4] * c_scale + off * inp[3]] ])
#
#        
#        B_k = torch.zeros((inp.shape[0], 2))
#        B_k[:5,:] = B_k_1
#        
#        grad_k = (grad_output.t().mm(B_k)) * time_step
#        #################################
#        #################################
#        pi_M1 = torch.eye(pi.shape[0])
#        B_pi = torch.zeros((inp.shape[0], pi.shape[0]))
#        B_pi[-pi.shape[0]:,:] = pi_M1
#
#        
#        grad_pi = (grad_output.t().mm(B_pi)).t() * time_step
#        #################################
#        #################################
#
#        return grad_inp, grad_W, grad_k, grad_pi, None, None, None, None




class Hybrid_layer(nn.Module):

    def __init__(self, n_real, n_fake, n_unseen, dt, t_scale, c_scale, function_type, nonlin, A, dev):
        super(Hybrid_layer, self).__init__()

        self.dev = dev

        self.n_real   = n_real
        self.n_fake   = n_fake
        self.n_unseen = n_unseen

        self.dt = dt
        self.t_scale = t_scale
        self.c_scale = c_scale
        self.A = A
        

        self.W_hx = torch.cat((torch.eye((n_real - n_fake)), torch.zeros((2 * n_fake + n_unseen, n_real -  n_fake))), axis=0).to(dev)
        self.W_hh = nn.Parameter(Uniform(-1, 1).sample((9 + n_unseen, 9 + n_unseen)))

        self.pi = nn.Parameter(Uniform(-1, 1).sample((n_unseen, 1)))

        if function_type == '1_pump':
            self.k  = nn.Parameter(Uniform(-2, 0).sample((4,)))
            self.hybrid_func = bartol_hybrid_1_pump.apply

        else:
            raise ValueError('Hybrid layer has been given a wrong function_type')


        if nonlin == 'Tanh':
            self.nonlin = nn.Tanh()

            ### GLOROT WEIGHT INITIALIZATION ###
            w_init = torch.tensor([2.45/(2*(n_real + n_fake + n_unseen))**0.5])
            b_init = torch.tensor([2.45/(2*(n_real + n_fake + n_unseen))**0.5])
        
        elif nonlin == 'ReLU':
            self.nonlin = nn.ReLU()

            ### KAIMING WEIGHT INITIALIZATION ###
            w_init = torch.tensor([1.41/(n_real + n_fake + n_unseen)**0.5])
            b_init = torch.zeros(self.pi.shape)

        else:
            raise ValueError('Hybrid layer has been given a wrong nonlinearity type')


        self.sig = torch.nn.Sigmoid()

        with torch.no_grad():

            self.W_hh[:,:] = self.W_hh * w_init
            self.pi[:,:] = self.pi * b_init
                
            #### STRUCTURAL CONSTRAINTS ####
            self.W_hh[:9, :9] = torch.eye(9)
            self.W_hh[:8, 9:] = torch.zeros((8, n_unseen))


    def forward(self, hh, xx):
        
        alpha = self.hybrid_func(hh.unsqueeze(-1), self.W_hh, self.k, self.pi, self.dt, self.t_scale, self.c_scale, self.dev) + torch.mm(self.W_hx, xx.unsqueeze(-1))

        out   = torch.cat((alpha[ : self.n_real,0], self.sig(alpha[self.n_real : (self.n_real + self.n_fake),0]), self.nonlin(alpha[-self.n_unseen : ,0]))).unsqueeze(-1)

        return out



###### MORE POWERFUL VERSION #####
class Lin_layer(nn.Module):

    def __init__(self, n_in, n_out, nonlin):
        super(Lin_layer, self).__init__()

        self.ww = nn.Parameter(Uniform(-1, 1).sample((n_out, n_in)))
        self.b  = nn.Parameter(Uniform(-1, 1).sample((n_out, 1)))

        if nonlin == 'ReLU':
            self.nonlinearity = nn.ReLU()
            w_scaling = torch.tensor([1.41/(n_in + n_out)**0.5])
            b_scaling = torch.zeros(self.b.shape)


        elif nonlin == 'Tanh':

            self.nonlinearity = nn.Tanh()
            w_scaling = torch.tensor([2.45/(2*(n_in + n_out))**0.5])
            b_scaling = torch.tensor([2.45/(2*(n_in + n_out))**0.5])


        with torch.no_grad():
            self.ww[:,:] = self.ww * w_scaling
            self.b[:,:]  = self.b  * b_scaling

        

    def forward(self, inputs):
        
        activation = (torch.mm(self.ww, inputs.unsqueeze(-1)) + self.b)
        
        return self.nonlinearity(activation)



class full_hybrid_model(nn.Module):

    def __init__(self, n_real, n_fake, n_unseen, dt, t_scale, c_scale, func_type, hybrid_nonlin, Lin_nonlin, Lin_num_layer, Lin_interm_NH, A, dev):
        super(full_hybrid_model, self).__init__()

        self.dev = dev

        self.n_real   = n_real
        self.n_fake   = n_fake
        self.n_unseen = n_unseen
        
        self.W_yh = torch.zeros((1, n_real + n_fake + n_unseen)).to(dev)
        self.W_yh[0,2] = 1


        self.h0 = nn.Parameter(Uniform(-1, 1).sample((n_fake + n_unseen,))).to(dev)

        with torch.no_grad():
            self.h0[:n_fake] = torch.abs(self.h0[:n_fake])

        self.Lin_list = []

        if Lin_num_layer >= 2:
            for i in range(0, Lin_num_layer):
            
                if i == 0:
                    exec('self.Lin_L_{} = Lin_layer(n_in = n_real + n_fake + n_unseen, n_out = Lin_interm_NH, nonlin = Lin_nonlin).to(dev)'.format(i))
                    exec('self.Lin_list.append(self.Lin_L_{})'.format(i))

                elif i == (Lin_num_layer - 1):
            
                    exec('self.Lin_L_{} = Lin_layer(n_in = Lin_interm_NH, n_out = n_unseen, nonlin = Lin_nonlin).to(dev)'.format(i))
                    exec('self.Lin_list.append(self.Lin_L_{})'.format(i))

                else:
                    exec('self.Lin_L_{} = Lin_layer(n_in = Lin_interm_NH, n_out = Lin_interm_NH, nonlin = Lin_nonlin).to(dev)'.format(i))
                    exec('self.Lin_list.append(self.Lin_L_{})'.format(i))
           

        elif Lin_num_layer == 1:
            exec('self.Lin_L_{} = Lin_layer(n_in = n_real + n_fake + n_unseen, n_out = n_unseen, nonlin = Lin_nonlin).to(dev)'.format(0))
            exec('self.Lin_list.append(self.Lin_L_{})'.format(0))


        self.hybrid = Hybrid_layer(n_real = n_real, n_fake = n_fake, n_unseen = n_unseen, dt = dt, t_scale = t_scale, c_scale = c_scale, function_type = func_type, nonlin = hybrid_nonlin, A = A, dev=dev).to(dev)


    def forward(self, h_prev, inputs):

        Lin_outputs = [h_prev,]

        for L in self.Lin_list:
            Lin_outputs.append(L(Lin_outputs[-1]).squeeze(dim=1))
           
        h_new = self.hybrid(torch.cat((h_prev[:self.n_real + self.n_fake], Lin_outputs[-1][-self.n_unseen:])), inputs)

        output = self.W_yh.mm(h_new).squeeze()

        return output, h_new.squeeze()


    def n_forward(self, n_steps, init_concs, input_vec, dev):
        
        y = torch.empty((1, n_steps))
        HH = torch.empty((self.n_real + self.n_fake + self.n_unseen, n_steps))

        init = init_concs.squeeze()

        hidden = torch.cat((init, self.h0))

        HH[:, 0] = hidden
        y[:,0]   = self.W_yh.mm(hidden.unsqueeze(-1)).squeeze()


        for i in range(1, n_steps):
            y[:, i], hidden = self.forward(hidden, inputs=input_vec[:,i])

            with torch.no_grad():
                HH[:, i] = hidden.detach().clone()

        return y, HH




class hybrid_object:

    def __init__(self, p):

        num_seen   = p['n_seen']
        num_bridge = p['n_bridge']
        num_unseen = p['n_unseen']

        self.num_seen   = p['n_seen']
        self.num_bridge = p['n_bridge']
        self.num_unseen = p['n_unseen']


        num_Lin_layers = p['N_Lin_Layers']
        
        if p['N_Lin_NH'] is not None: 
            num_Lin_NH = p['N_Lin_NH']
        else:
            num_Lin_NH = num_seen + num_bridge + num_unseen

        dim_w = num_seen + num_bridge + num_unseen
        
        self.ww_mask = torch.ones((dim_w, dim_w))
        self.ww_mask[:num_seen + num_bridge, :num_seen + num_bridge] = torch.zeros((num_seen + num_bridge, num_seen + num_bridge))
        self.ww_mask[:num_seen, num_seen + num_bridge:]              = torch.zeros((num_seen, num_unseen))

        self.dev = p['dev']

        self.net = full_hybrid_model(n_real = num_seen, n_fake = num_bridge, n_unseen = num_unseen, dt = p['dt'], t_scale = p['t_scale'], c_scale = p['c_scale'], func_type = '1_pump', hybrid_nonlin = 'Tanh', Lin_nonlin = 'Tanh', Lin_num_layer = num_Lin_layers, Lin_interm_NH = num_Lin_NH, A=1, dev = self.dev)

        self.c_scale = p['c_scale']

#        self.opt = p['opt'](self.netparameters(), lr = p['lr'], weight_decay = p['dec_lam'])
#        self.sch = p['sch'](self.opt, step_size = p['SCH_STEP_SIZE'], gamma = p['SCH_GAMMA']

        self.train_losses_list = []
        self.train_batch_losses_list = []

        self.val_losses_list = []
        self.val_batch_losses_list = []



    def train_model(self, train_in, train_out, t, y_0, N_EPS, N_BATCHES, BATCH_SIZE, opt, sch, n_cores, val_in = None, val_out = None):

        y_0 = torch.from_numpy(y_0).type(torch.float32)

#        ep_losses_list = []
#        batch_losses_list = []
#
#        val_ep_losses_list = []
#        val_batch_losses_list = []

        
        if val_in is not None:
            val_in  = [torch.from_numpy(dat).type(torch.float32)  for dat in val_in]
            val_out = [torch.from_numpy(dat).type(torch.float32)[None,:] for dat in val_out]


        for ep in range(N_EPS):


            base_idx = np.arange(0, len(train_in), 1, dtype=int)
            np.random.shuffle(base_idx)

            batch_loss_list = []
            val_loss_list = []

            for bat in range(N_BATCHES):

                t0 = time.time()

                print('EP {}/{};    BATCH {}/{}'.format(ep + 1, N_EPS, bat + 1, N_BATCHES))

                
                b_idx = base_idx[bat * BATCH_SIZE : (bat + 1) * BATCH_SIZE]

                bat_in  = [torch.from_numpy(train_in[idx]).type(torch.float32)  for idx in b_idx]
                bat_out = [torch.from_numpy(train_out[idx]).type(torch.float32)[None,:] for idx in b_idx]

                rnn_out = []

                for n in range(BATCH_SIZE):

                    y_rnn, h = self.net.n_forward(n_steps = bat_out[n].shape[1], init_concs = y_0 / self.c_scale, input_vec = bat_in[n][: (self.num_seen - self.num_bridge),:]  , dev = self.dev)

                    rnn_out.append(y_rnn)



                batch_loss = torch.sum(torch.trapz(torch.square(torch.cat(bat_out, axis = 0) - torch.cat(rnn_out, axis = 0)), x = torch.from_numpy(t).type(torch.float32)))


                batch_loss_list.append(batch_loss.item())


                self.net.zero_grad()
                print('Calculating gradients')
                batch_loss.backward()


                with torch.no_grad():
                    
                    thresh = 0

                    for name, tens in self.net.named_parameters():
        #                print(name)
        #                print(tens.grad)
                        if name != 'hybrid.W_hh':
                            if torch.norm(tens.grad, p='fro') > thresh:
                                thresh = torch.norm(tens.grad, p='fro')


                    for name, tens in self.net.named_parameters():
                        if name == 'hybrid.W_hh':
                            tens.grad = tens.grad * self.ww_mask

                            if torch.norm(tens.grad, p='fro') >= thresh:
                                tens.grad = thresh / torch.norm(tens.grad, p='fro') * tens.grad

                opt.step()
                sch.step()

                t1 = time.time()

                print('Time is: {}'.format(t1 - t0))

                rnn_val_out = []
                
                if val_in is not None:


                    for n_val in range(len(val_in)):
                        with torch.no_grad():

                            y_val, h = self.net.n_forward(n_steps = val_out[n_val].shape[1], init_concs = y_0 / self.c_scale, input_vec = val_in[n_val][: (self.num_seen - 2),:], dev = self.dev)
                            
                            rnn_val_out.append(y_val)

#                            print(y_val.shape)
#                            print(val_out[n].shape)

#                            plt.plot(y_val.detach().clone().T)
#                            plt.plot(val_out[n].T)
#
#                            plt.show()
                

                #val_batch_loss = torch.mean(torch.trapz(torch.square(torch.cat(val_out, axis = 0) - torch.cat(rnn_val_out, axis = 0)), x = torch.from_numpy(t).type(torch.float32)))
                if val_in is not None:
                    val_batch_loss = torch.sum(torch.trapz(torch.square(torch.cat(val_out, axis = 0) - torch.cat(rnn_val_out, axis = 0)), x = torch.from_numpy(t).type(torch.float32)))
                    val_loss_list.append(val_batch_loss.item())

                #print('BATCH LOSS: {};      VAL LOSS: {}'.format(np.sqrt(batch_loss.item()/BATCH_SIZE), np.sqrt(val_batch_loss.item()/len(val_in))))
                if val_in is not None:
                    print('BATCH LOSS: {};      VAL LOSS: {}'.format(batch_loss.item(), val_batch_loss.item()))
                else:
                    print('BATCH LOSS: {}'.format(batch_loss.item()))




            self.train_batch_losses_list.append(batch_loss_list)
            self.train_losses_list.append(np.mean(batch_loss_list))


            if val_in is not None:
                self.val_batch_losses_list.append(val_loss_list)
                self.val_losses_list.append(np.mean(val_loss_list))



        if val_in is not None:
            return self.train_losses_list, self.train_batch_losses_list, self.val_losses_list, self.val_batch_losses_list
        else:
            return self.train_losses_list, self.train_batch_losses_list, [], []




    def eval_inputs(self, ins, outs, t, y_0):

        y_0 = torch.from_numpy(y_0).type(torch.float32)

        ins  = [torch.from_numpy(dat).type(torch.float32)  for dat in ins]
        outs = [torch.from_numpy(dat).type(torch.float32)[None,:] for dat in outs]

        rnn_out = []

        with torch.no_grad():
            for n in range(len(ins)):
                y_rnn, h = self.net.n_forward(n_steps = outs[n].shape[1], init_concs = y_0 / self.c_scale, input_vec = ins[n][: (self.num_seen - self.num_bridge),:], dev = self.dev)
                            
                rnn_out.append(y_rnn)


        loss = torch.mean(torch.trapz(torch.square(torch.cat(outs, axis = 0) - torch.cat(rnn_out, axis = 0)), x = torch.from_numpy(t).type(torch.float32)))

        rnn_out = [out.detach().cpu().numpy() for out in rnn_out]

        return loss.item(), rnn_out



    def save_net(self, save_name):

        stuff_to_save = {'net':self.net.state_dict(), 'train_loss':self.train_losses_list, 'train_batch_loss':self.train_batch_losses_list, 'val_loss':self.val_losses_list, 'val_batch_loss':self.val_batch_losses_list}

        torch.save(stuff_to_save, save_name)

        return 0



    def load_net(self, f_name = None, dict_name = None, weights=None):

        if f_name is not None:
            stuff = torch.load(f_name)
            weights = stuff['net']

        elif dict_name is not None:
            weights = dict_name['net']
        else:
            print('Neither file name or variable has been given to load into the network')

        self.net.load_state_dict(weights)

        return 0
