import numpy as np
import pandas as pd

from joblib import Parallel, delayed
from scipy.integrate import solve_ivp


def f_bartol(t, y, p, c):
    
    k1f = p['k1f']
    k1b = p['k1b']
    
    k2f = p['k2f']
    k2b = p['k2b']
    
    k3f = p['k3f']
    k3b = p['k3b']
    
    k4f = p['k4f']
    k4b = p['k4b']
    k4p = p['k4p']
    k4l = p['k4l']

    k5f = p['k5f']
    k5b = p['k5b']
    k5p = p['k5p']
    k5l = p['k5l']


    Ca      = y[0]
    Fluo4   = y[1]
    CaFluo4 = y[2]
    OGB1    = y[3]
    CaOGB1  = y[4]
    IMCBP   = y[5]
    CaIMCBP = y[6]
    NCX     = y[7]
    CaNCX   = y[8]
    PMCA    = y[9]
    CaPMCA  = y[10]


    dCa      = -k1f * Ca * Fluo4 * c + k1b * CaFluo4 - k2f * Ca * OGB1 * c + k2b * CaOGB1 - k3f * Ca * IMCBP * c + k3b * CaIMCBP - k4f * Ca * NCX * c + k4b * CaNCX + k4l * NCX - k5f * Ca * PMCA * c + k5b * CaPMCA + k5l * PMCA
    dFluo4   = -k1f * Ca * Fluo4 * c + k1b * CaFluo4
    dCaFluo4 =  k1f * Ca * Fluo4 * c - k1b * CaFluo4
    dOGB1    = -k2f * Ca * OGB1  * c + k2b * CaOGB1 
    dCaOGB1  =  k2f * Ca * OGB1  * c - k2b * CaOGB1 
    dIMCBP   = -k3f * Ca * IMCBP * c + k3b * CaIMCBP
    dCaIMCBP =  k3f * Ca * IMCBP * c - k3b * CaIMCBP
    dNCX     = -k4f * Ca * NCX   * c + (k4b + k4p) * CaNCX
    dCaNCX   =  k4f * Ca * NCX   * c - (k4b + k4p) * CaNCX
    dPMCA    = -k5f * Ca * PMCA  * c + (k5b + k5p) * CaPMCA
    dCaPMCA  =  k5f * Ca * PMCA  * c - (k5b + k5p) * CaPMCA


    return np.array([dCa, dFluo4, dCaFluo4, dOGB1, dCaOGB1, dIMCBP, dCaIMCBP, dNCX, dCaNCX, dPMCA, dCaPMCA])



def RK45_with_input(system, y0, t_max, dt, evals, params, c_scale, index, inp=None):

    if index is not None:
        print('Index is {}'.format(index))
    
    #TODO:subsample to a given granularity

    T = np.arange(0.0, float(t_max)+dt, dt)

    Y = np.zeros((y0.shape[0], T.shape[0]))
    Y[:,0] = y0.squeeze()/c_scale

    for n in range(1, T.shape[0]):

        t_n = T[n]
        
        k1 = system(t=t_n,        y=Y[:,n-1],             p=params, c=c_scale) 
        k2 = system(t=t_n + dt/2, y=Y[:,n-1] + dt/2 * k1, p=params, c=c_scale) 
        k3 = system(t=t_n + dt/2, y=Y[:,n-1] + dt/2 * k2, p=params, c=c_scale)
        k4 = system(t=t_n + dt  , y=Y[:,n-1] + dt   * k3, p=params, c=c_scale)

        try:
            Y[:,n] = Y[:,n-1] + 1.0/6.0 * dt * (k1 + 2*k2 + 2*k3 + k4) + inp[:,n] #/ c_scale
        except TypeError:
            Y[:,n] = Y[:,n-1] + 1.0/6.0 * dt * (k1 + 2*k2 + 2*k3 + k4)

    return T, inp, Y 




def gen_toy_exp_perturb(system, k_vals, y_init_func, t_max, dt, norm_conc, v2_r, td1_r, td2_r, tau1_r, tau2_r, num_runs, n_cores):

    ode_init_list = []
    rnn_init_list = []
    in_list       = []
    out_list      = []
    plain_rnn_fluo_init = []

    t = np.arange(0, t_max + dt, dt)

    for i in range(0, num_runs):

        stim_no = np.random.choice(a=[0, 1, 2, 3], size=1)[0]

#        stim_no = 3
#        print('exp stim_no is {}'.format(stim_no))

        if stim_no == 0:
            ode_init, rnn_init = y_init_func(which='Fluo4', conc='20')
            plain_rnn_fluo_init.append(np.array([20, 0]) / norm_conc) 

        if stim_no == 1:
            ode_init, rnn_init = y_init_func(which='OGB1', conc='20')
            plain_rnn_fluo_init.append(np.array([0, 20]) / norm_conc) 

        if stim_no == 2:
            ode_init, rnn_init = y_init_func(which='OGB1', conc='50')
            plain_rnn_fluo_init.append(np.array([0, 50]) / norm_conc) 

        if stim_no == 3:
            ode_init, rnn_init = y_init_func(which='OGB1', conc='100')
            plain_rnn_fluo_init.append(np.array([0, 100]) / norm_conc) 


        ode_init_list.append(ode_init)
        rnn_init_list.append(rnn_init)

        v1  = 0
        v2  = np.random.uniform(low=v2_r[0],  high=v2_r[1],  size=1)

        td1 = np.random.uniform(low=td1_r[0], high=td1_r[1], size=1)
        td2 = np.random.uniform(low=td1,      high=td1 + td2_r, size=1)

        td1_i = int(td1/dt)
        td2_i = int(td2/dt)

        tau1 = np.random.uniform(low=tau1_r[0], high=tau1_r[1], size=1)
        tau2 = np.random.uniform(low=tau2_r[0], high=tau2_r[1], size=1)
        

        in_concs = np.zeros((ode_init.shape[0], int(t_max/dt) + 1)) 

        in_concs[0, 0 : td1_i]     = v1 * np.ones((1, td1_i))
        in_concs[0, td1_i : td2_i] = v1 + (v2-v1) * (1 - np.exp(-(t[td1_i : td2_i] - td1) / tau1))
        in_concs[0, td2_i :]       = v1 + (v2-v1) * (1 - np.exp(-(td2 - td1) / tau1)) * np.exp(-(t[td2_i:] - td2) / tau2) 

        in_concs = in_concs / norm_conc
        
        in_list.append(in_concs)


        

    stuff = Parallel(n_jobs=n_cores)(delayed(RK45_with_input)(system=system, params = k_vals, y0 = ode_init_list[i], t_max=t_max, dt=dt, evals=None, c_scale=norm_conc, inp = in_list[i], index=i) for i in range(len(in_list)))

    t = stuff[0][0]
    in_list  = [thing[1] for thing in stuff]
    out_list = [thing[2] for thing in stuff]


    return t, ode_init_list, rnn_init_list, in_list, out_list, plain_rnn_fluo_init






def gen_toy_delta_perturb(system, k_vals, y_init_func, t_max, dt, norm_conc, n_delta_low, n_delta_high, c_delta_low, c_delta_high, t_delta_low, t_delta_high, num_runs, n_cores):

    ode_init_list = []
    rnn_init_list = []
    in_list       = []
    out_list      = []
    plain_rnn_fluo_init = []

    t = np.arange(0, t_max + dt, dt)

    for i in range(0, num_runs):

        stim_no = np.random.choice(a=[0, 1, 2, 3], size=1)[0]

#        stim_no = 3
#        print('delta stim_no is {}'.format(stim_no))

        if type(y_init_func) != np.ndarray:
            if   stim_no == 0:
                ode_init, rnn_init = y_init_func(which='Fluo4', conc='20')
                plain_rnn_fluo_init.append(np.array([20, 0]) / norm_conc) 

            elif stim_no == 1:
                ode_init, rnn_init = y_init_func(which='OGB1', conc='20')
                plain_rnn_fluo_init.append(np.array([0, 20]) / norm_conc) 

            elif stim_no == 2:
                ode_init, rnn_init = y_init_func(which='OGB1', conc='50')
                plain_rnn_fluo_init.append(np.array([0, 50]) / norm_conc) 

            elif stim_no == 3:
                ode_init, rnn_init = y_init_func(which='OGB1', conc='100')
                plain_rnn_fluo_init.append(np.array([0, 100]) / norm_conc) 
        
        else:

            ode_init = y_init_func
            rnn_init = np.hstack([ode_init[:-4], np.array([0,])]) 
            
            if ode_init[1] > 0:
                plain_rnn_fluo_init.append(np.array([ode_init[1] + ode_init[2], 0]) / norm_conc)

            elif ode_init[3] > 0:
                plain_rnn_fluo_init.append(np.array([0, ode_init[3] + ode_init[4]]) / norm_conc)



        ode_init_list.append(ode_init)
        rnn_init_list.append(rnn_init)


        num_d = np.random.uniform(low=n_delta_low, high=n_delta_high, size=1)
        concs = np.random.uniform(low=c_delta_low, high=c_delta_high, size=int(num_d)) 
        times = np.random.choice(np.arange(t_delta_low, t_delta_high + dt, dt), size = int(num_d), replace = False) 

        times_ind = (times/dt).astype(int)

        in_concs = np.zeros((ode_init.shape[0], int(t_max/dt) + 1)) 
        in_concs[0, times_ind] = concs / norm_conc
        in_list.append(in_concs)



    stuff = Parallel(n_jobs=n_cores)(delayed(RK45_with_input)(system=system, params = k_vals, y0 = ode_init_list[i], t_max = t_max, dt = dt, evals = None, c_scale = norm_conc, inp = in_list[i], index=i) for i in range(len(in_list)))

    t = stuff[0][0]
    in_list = [thing[1] for thing in stuff]
    out_list = [thing[2] for thing in stuff]
    

    return t, ode_init_list, rnn_init_list, in_list, out_list, plain_rnn_fluo_init



#from matplotlib import pyplot as plt

def gen_range_with_pre_sim(system, conc_min, conc_max, conc_step, which_buffer, n_sims, c_norm, c_0_pre_sim, dp, n_cores):

    ode_init_list = []
    rnn_init_list = []
    in_list       = []
    out_list      = []
    plain_rnn_fluo_init = []

    t = np.arange(0, dp['t_max'] + dp['dt'], dp['dt'])

    concs = np.arange(conc_min, conc_max + conc_step, conc_step)

    for conc in concs:
        print(conc)

        if which_buffer == 'OGB1':
            c_0_i = c_0_pre_sim
            c_0_i[3] = conc

        elif which_buffer == 'Fluo4':
            
            c_0_i = c_0_pre_sim
            c_0_i[1] = conc


        t0 = 0
        t_max = 3000#000
        dt = t_max / 10.0
#        t  = np.arange(t0, t_max + dt, dt)

        sol = solve_ivp(fun = system, t_span = (t0, t_max), y0 = c_0_i, args = [dp['k_vals_unscaled'], 1.0]) #, t_eval = t)

        c_0 = sol.y[:,-1]
#        print(c_0)
#        plt.plot(t, sol.y[0,:], label='Ca')
#        plt.plot(t, sol.y[3,:], label='OGB1')
#        plt.plot(t, sol.y[4,:], label='CaOGB1')
#        plt.plot(t, sol.y[1,:], label='Fluo4')
#        plt.plot(t, sol.y[2,:], label='CaFluo4')
#
#        plt.legend()
#        plt.show()

        stuff = gen_toy_delta_perturb(system = f_bartol, k_vals = dp['k_vals_scaled'], y_init_func = c_0, t_max = dp['t_max'], dt = dp['dt'], norm_conc = dp['c_scale'], n_delta_low = 0, n_delta_high = 10, c_delta_low = 0.1, c_delta_high = 10, t_delta_low = 15 / dp['t_scale'], t_delta_high = 85 / dp['t_scale'], num_runs = n_sims , n_cores = n_cores)

        ode_init_list.extend(stuff[1])
        rnn_init_list.extend(stuff[2])
        in_list.extend(stuff[3])
        out_list.extend(stuff[4])
        plain_rnn_fluo_init.extend(stuff[5])
        
#        plt.plot(stuff[0], stuff[4][0][0,:], label='Ca')
#        plt.plot(stuff[0], stuff[4][0][1,:], label='Fluo4')
#        plt.plot(stuff[0], stuff[4][0][2,:], label='CaFluo4')
#        plt.plot(stuff[0], stuff[4][0][3,:], label='OGB1')
#        plt.plot(stuff[0], stuff[4][0][4,:], label='CaOGB1')
#        plt.legend()
#        plt.show()


    return t, ode_init_list, rnn_init_list, in_list, out_list, plain_rnn_fluo_init
