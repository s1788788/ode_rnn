from ODE_code_bartol import *
from matplotlib import pyplot as plt
from scipy.integrate import solve_ivp

import numpy as np

Ca_0        =  0.1  ##uM
Fluo4_0     =  0.0  ##uM
CaFluo4_0   =  0.0  ##uM
OGB1_0      = 67.6  ##uM
CaOGB1_0    = 32.4  ##uM
IMCBP_0     =75.54  ##uM
CaIMCBP_0   =  3.5  ##uM
NCX_0       =  2.0  ##uM
CaNCX_0     =  1.0  ##uM
PMCA_0      = 18.4  ##uM
CaPMCA_0    =  0.6  ##uM

init_conc = np.array([Ca_0, Fluo4_0, CaFluo4_0, OGB1_0, CaOGB1_0, IMCBP_0, CaIMCBP_0, NCX_0, CaNCX_0, PMCA_0, CaPMCA_0])

#norm_conc = np.max(init_conc)
norm_conc = 1

t_scale = 0.01

rates     = {'k1f' : 0.8 * t_scale,    'k1b' : 0.24 * t_scale,
             'k2f' : 0.8 * t_scale,    'k2b' : 0.16 * t_scale,
             'k3f' : 0.247  * t_scale, 'k3b' : 0.524 * t_scale,
             'k4f' : 0.15   * t_scale, 'k4b' : 0.015 * t_scale, 'k4p' : 0.012 * t_scale, 'k4l' : 0.0043 * t_scale,
             'k5f' : 0.3    * t_scale, 'k5b' : 0.30  * t_scale, 'k5p' : 0.6   * t_scale, 'k5l' : 0.0194 * t_scale} # 1/(uM * ms)


evals = np.arange(0, 1000+0.01, 0.01)
#sol = solve_ivp(fun = f_bartol, t_span = (0, 1000), y0 = init_conc, args = [rates, None], t_eval = evals)
            

T, I, Y = RK45_with_input(system = f_bartol, y0 = init_conc, t_max = 1000/t_scale, dt = 0.01/t_scale, evals = None, params = rates, c_scale = norm_conc, inp = None, index=1)

#plt.plot(evals, sol.y[0, :], label='solver')
plt.plot(evals, Y[0, :], label='Ca')

plt.plot(evals, Y[1, :], label='Fluo4')
plt.plot(evals, Y[2, :], label='CaFluo4')

plt.plot(evals, Y[3, :], label='OGB1')
plt.plot(evals, Y[4, :], label='CaOGB1')

plt.plot(evals, Y[5, :], label='IMCBP')
plt.plot(evals, Y[6, :], label='CaIMCBP')

#plt.plot(evals, Y[7, :], label='NCX')
#plt.plot(evals, Y[8, :], label='CaNCX')
#
#plt.plot(evals, Y[9, :], label='PMCA')
#plt.plot(evals, Y[10, :], label='CaPMCA')

plt.legend()
plt.show()


#import time 
#
#t0 = time.time()
#
#t, ins, outs = gen_toy_delta_perturb(system=f_bartol, k_vals = rates, y_init = init_conc, t_max = 1000, dt = 0.01, norm_conc = 1, n_delta_low = 1, n_delta_high = 20, c_delta_low = 0.5, c_delta_high = 5, t_delta_low = 50, t_delta_high = 800, num_runs = 10, n_cores = 10)
#
#t1 = time.time()
#
#print(t1 - t0)
#
#
#plt.plot(t, outs[0][0,:])
#plt.plot(t, ins[0][0,:])
#plt.show()
